<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\{Storage};
use Emploi\User;

class RouteTest extends TestCase
{
    protected $user;

    public function setUp(){
    	parent::setUp();
    	$this->user = factory(User::class)->create();
    }

    public function testManageSales()
    {
    	$response = $this->actingAs($this->user)
    					 ->get("/sales/manage");
      
        $response->assertSuccessful();
    }

    public function testAddFunds(){
    	$response = $this->actingAs($this->user)->json("POST","/payments/addfunds",[
          "cardNo" => "5061020000000000094",
          "cvv" => "347",
          "expiryMonth" => "07",
          "expiryYear" => "20",
          "pin" => "1111",
          "amount" => "2000",
          "otp" => "123456",
          "ref" => "",
    	]);

    	$response->assertSuccessful();
    }

    public function testAddFundsView(){
    	$response = $this->actingAs($this->user)->get("payments/addfunds");
    	$response->assertSuccessful();
    }

    // public function testWithdrawalView(){
    // 	$response = $this->actingAs($this->user)->get("payments/withdraw");
    // 	$response->assertSuccessful();
    // }

   	public function testPostRequest(){
   		Storage::fake('test');
   		Storage::disk('test')->makeDirectory('attachments');
   		$fakeAttachment = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->actingAs($this->user)
        				 ->json('POST','/requests/new', [
					        	"description" => "Test description",
					    		"category" => 1,
					    		"sub-category" => 1,
					    		"delivery-period" => 3,
					    		"budget" => 4000,
					            'attachment' => $fakeAttachment,
					        ]);

        //Storage::disk('public')->assertExists('attachments/avatar.jpg');
        $response->assertStatus(302);
   	}
}
