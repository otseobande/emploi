# About Emploi

Emploi is the leading indiginous Nigerian freelance workspace linking employers of services to employees that render need service

# Requirement

- PHP >= 7.0
- Git
- Mysql
- Composer
- Node/Npm


# Installation

To install this application you need to run through the following steps:

- Open your terminal (Command prompt on windows)
- Run 'git clone https://otseobande@bitbucket.org/otseobande/emploi.git' in the root directory you want to host the project
- Navigate into the newly created 'emploi' folder/directory
- Run 'Composer install' to install dependencies
- Create a mysql database called emploi
- Run 'php artisan migrate' to migrate database tables
- Run 'php artisan serve' to start the php server for the application or access the application through public/index.php on apache or Nginx


For further enquires please contact otseobande@gmail.com.