<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@customLogin');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::prefix('/sales')->group(function(){
	Route::get('/manage','OrdersController@manageSales');
});

Route::prefix('/payments')->group(function(){
	Route::get('/addfunds','Payment\DepositController@addFundsView');
	Route::post('/addfunds','Payment\DepositController@addFunds');
	Route::get('/withdraw','Payment\WithdrawalController@withdrawalView');
	Route::post('/withdraw','Payment\WithdrawalController@withdraw');
	Route::get('/addaccount', 'Payment\WithdrawalController@addBankAccountView');
	Route::post('/addaccount', 'Payment\WithdrawalController@addBankAccount');
	Route::post('/validate','Payment\DepositController@validateCard');
	
});

Route::prefix('/gigs')->group(function(){
	Route::get('/manage', 'GigController@index')->name('gigs');
	Route::get('/create','GigController@createView')->name('create-gig');
	Route::post('/create','GigController@create');
	Route::get('/edit/{gig}','GigController@editView');
	Route::post('/edit/{gig}','GigController@edit');
	Route::post('/pause/{gig}', 'GigController@pause');
	Route::post('/activate/{gig}', 'GigController@activate');
	Route::post('/cancel/{gig}', 'GigController@cancel');
});


Route::get('/search','SearchController@search')->name('search');

Route::get('/search/{q}','SearchController@search');

Route::get('/dashboard','DashboardController@index')->name('dashboard');

Route::prefix('/conversations')->group(function(){
	Route::get('/', 'InboxController@index')->name('inbox');
	Route::get('/{user}','InboxController@conversation');
	Route::post('/{user}/send','InboxController@sendMessage');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create_categories','CategoriesController@createCategories');
Route::get('/create_subcategories','CategoriesController@createSubCategories');

Route::prefix('/profile')->group(function(){
	Route::post('/changeprofilepicture', 'ProfileController@changeProfilePicture');
	Route::get('/{user}','ProfileController@index');
	Route::post('/edit/description','ProfileController@editDescription');
	Route::post('/add/skill','ProfileController@addSkill');
});


Route::get('/team-account','UserController@teamAccountView');


Route::prefix('/requests')->group(function(){
	Route::get('/new','RequestController@newRequestView');
	Route::post('/new','RequestController@postNewRequest');
	Route::get('{request}/offer','RequestController@makeOfferView');
	Route::post('/offer/make','RequestController@makeOffer');
	Route::post('/offer/{request_id}/cancel','RequestController@cancelOffer');
	Route::post('/offer/accept/{offer}','OrdersController@acceptOffer');
	Route::get('/manage','RequestController@manageRequests')->name('manage-requests');
	Route::get('/manage/{request}','RequestController@viewOffers');
	Route::get('/all','RequestController@all');
	Route::post('/suspend/{request}','RequestController@suspend');
	Route::post('/activate/{request}','RequestController@activate');
});

Route::post('/offer/accept/{offer}','OrdersController@acceptCustomOffer');
Route::post('/offer/withdraw/{offer}','OfferController@withdrawCustomOffer');

Route::prefix('/orders')->group(function(){
	Route::get('/manage','OrdersController@manageOrders');
	Route::get('/gig/{gig}','OrdersController@viewGig');
	Route::post('/gig/{gig}','OrdersController@orderGig');
	Route::post('/deliver/{order}','OrdersController@deliverOrder');
	Route::post('/cancel/{order}','OrderController@cancelOrder');
	Route::post('/requestmodification/{order}','OrdersController@requestModification');
});

Route::get('/categories/{category}','CategoriesController@searchView');
Route::get('/notifications','NotificationController@view');

Route::get('/download/{path}','DownloadController@download');

Route::post('/removeskill/{skill}','ProfileController@removeSkill');

Route::get('/settings','SettingsController@index');
Route::post('/settings/changepassword','SettingsController@changepassword');

Route::post('/customoffer/create','OfferController@makeCustomOffer');

