<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class UserCertification extends Model
{
    //
	public function user(){
		return $this->belongsTo('Emploi\User');
	}
}
