<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class GigImage extends Model
{
    //
    public function gig(){
    	return $this->belongsTo('Emploi\Gig');
    }
}
