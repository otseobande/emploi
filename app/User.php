<?php

namespace Emploi;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Authenticatable
{
    use Notifiable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 
        'password', 'bio','avatar',
        'phone', 'address','skills',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRouteKeyName(){
        return 'username';
    }

    public function gigs(){
        return $this->hasMany('Emploi\Gig');
    }

    public function orders(){
        return $this->hasMany('Emploi\Order', 'employer_id');
    }

    public function certifications(){
        return $this->hasMany('Emploi\UserCertification');
    }

    public function education(){
        return $this->hasMany('Emploi\UserEducation');
    }

    public function skills(){
        return $this->hasMany('Emploi\UserSkill');
    }

    public function requests(){
        return $this->hasMany('Emploi\EmployerRequest');
    }

    public function offers(){
        return $this->hasMany('Emploi\Offer');
    }

    public function bankDetails(){
        return $this->hasMany('Emploi\BankDetail');
    }

    public function reviews(){
        return $this->hasMany('Emploi\Review', 'reciever_id');
    }

    public function ratings(){
        return $this->reviews()->avg('rating') ?? 0;
    }

    public function myCustomOffers(){
        return $this->hasMany('Emploi\CustomOffer');
    }

    public function employeeCustomOffers(){
        return $this->hasMany('Emploi\CustomOffer', 'employer_id');
    }
}
