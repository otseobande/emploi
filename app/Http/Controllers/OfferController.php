<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\{CustomOffer, Offer};

class OfferController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

	public function makeCustomOffer(Request $request){
		$offer = new CustomOffer();
    	$offer->user_id = Auth::user()->id;
    	$offer->employer_id = $request->input('employer-id');
    	$offer->gig_id = $request->input('gig-id');
    	$offer->details = $request->input('details');
    	$offer->price = $request->input('price');
    	$offer->delivery_period = $request->input('delivery-period');
    	$offer->save();

    	return back();
    	return ['message' => 'Custom Offer created successfully!'];
	}

	public function acceptCustomOffer(Request $request, CustomOffer $offer){
		$offer->accepted = 1;
		$offer->save();

		return ['message' => 'Custom Offer accepted'];
	}

	public function withdrawCustomOffer(Request $request, CustomOffer $offer){
		$offer->delete();

		return ['message' => 'Offer withdrawn'];
	}
    public function makeOffer(Request $request){
    	$offer = new Offer();
    	$offer->user_id = Auth::user()->id;
    	$offer->employer_request_id = $request->input('request-id');
    	$offer->gig_id = $request->input('gig-id');
    	$offer->details = $request->input('details');
    	$offer->price = $request->input('amount');
    	$offer->delivery_period = $request->input('delivery-period');
    	$offer->save();

    	return ['message' => 'Offer created successfully!'];
    }
}
