<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\User;

class DashboardController extends Controller
{
    //
    function __construct(){
    	$this->middleware('auth');
    }


    function index(){
    	//$avatar = Auth::user()->avatar;
  
    	return view('dashboard');
    }
}
