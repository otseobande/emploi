<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\Category;
use Emploi\Gig;

class GigController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}
    public function index(){
    	$active_gigs = Auth::user()->gigs()
                                    ->where('active',1)
                                    ->orderBy('created_at','desc')
                                    ->get();
        $paused_gigs = Auth::user()->gigs()
                                   ->where('active',0)
                                   ->where('cancelled',0)
                                    ->orderBy('created_at','desc')
                                   ->get();
        $cancelled_gigs = Auth::user()->gigs()
                                     ->where('cancelled',1)
                                      ->orderBy('created_at','desc')
                                     ->get();
    	return view('manage-gigs', compact('active_gigs','paused_gigs','cancelled_gigs'));
    }

    public function createView(){
    	$categories = Category::all();
        $categories_arr = [];
   
        foreach($categories as $category){
            $temp_arr = [];
            $temp_arr['name'] = $category->name;
            $temp_arr['id'] = $category->id;
            $temp_arr['subcategories'] = [];
            

            foreach($category->subcategories()->get() as $subcategory){
                $temp_subcategory_arr = [];
                $temp_subcategory_arr['name'] = $subcategory->name;
                $temp_subcategory_arr['id'] = $subcategory->id;
                array_push($temp_arr['subcategories'],$temp_subcategory_arr);
            }
            array_push($categories_arr,$temp_arr);
        }

        $json_categories = json_encode($categories_arr);
    	return view('create-gig',compact('json_categories'));
    }

    public function create(Request $request){
    	$gig = new Gig;

    	$gig->title = "I will ".$request->input('gig-title');
    	$gig->category_id =  $request->input('category');
        $gig->sub_category_id = $request->input('sub-category');
    	$gig->user_id = Auth::user()->id;
    	$gig->description = $request->input('description');
    	$gig->delivery_period = $request->input('delivery-period');
    	$gig->price = $request->input('price');
    	$gig->requirements = $request->input('requirements');

    	$gig->image = $request->file('image')->store('img/gigs');
        $gig->active = 1;

    	$gig->save();

    	return redirect('gigs/manage');
    }

    public function editView(Request $request, Gig $gig){
        $categories = Category::all();
        $categories_arr = [];
         foreach($categories as $category){
            $temp_arr = [];
            $temp_arr['name'] = $category->name;
            $temp_arr['id'] = $category->id;
            $temp_arr['subcategories'] = [];
            

            foreach($category->subcategories()->get() as $subcategory){
                $temp_subcategory_arr = [];
                $temp_subcategory_arr['name'] = $subcategory->name;
                $temp_subcategory_arr['id'] = $subcategory->id;
                array_push($temp_arr['subcategories'],$temp_subcategory_arr);
            }
            array_push($categories_arr,$temp_arr);
        }

        $json_categories = json_encode($categories_arr);
        return view('edit-gig', compact('gig','json_categories'));
    }

    public function edit(Request $request, Gig $gig){
        $gig->title = $request->input('gig-title');
        $gig->category_id =  $request->input('category');
        $gig->sub_category_id = $request->input('sub-category');
        $gig->user_id = Auth::user()->id;
        $gig->description = $request->input('description');
        $gig->delivery_period = $request->input('delivery-period');
        $gig->price = $request->input('price');
        $gig->requirements = $request->input('requirements');

        //$gig->image = $request->file('image')->store('img/gigs');
        $gig->active = 1;

        $gig->save();

        return redirect('gigs');
    }

    public function pause(Gig $gig){
        $gig->active = 0;
        $gig->save();

        return ["message" => "Gig cancelled."];
    }

    public function activate(Gig $gig){
        $gig->active = 1;
        $gig->save();

        return ["message" => "Gig activated."];
    }

    public function cancel(Gig $gig){
        $gig->cancelled = 1;
        $gig->active = 0;
        $gig->save();

        return ["message" => "Gig cancelled"];
    }
}
