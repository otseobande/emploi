<?php

namespace Emploi\Http\Controllers\Payment;

use Illuminate\Http\Request;
use Emploi\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Emploi\{Payment,User};

class StatusController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function status(){
    	
        $payments = Payment::where('user_id',Auth::user()->id)->paginate(20);
        
    	return view('paymentsummary',compact('payments'));
    }
}
