<?php

namespace Emploi\Http\Controllers\Payment;

use Emmanix2002\Moneywave\Enum\PaymentMedium;
use Emmanix2002\Moneywave\Enum\Environment;
use Emmanix2002\Moneywave\Enum\AuthorizationType;
use Emmanix2002\Moneywave\Exception\ValidationException;
use Emmanix2002\Moneywave\Moneywave;

use Emploi\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Emploi\AccountDetails;
use Emploi\BankDetail;
use Emploi\{Payment,PaymentDetail} ;

class WithdrawalController extends Controller
{
	private $merchantKey;
	private $apiKey;
	private $env; //can be staging or production

    public function __construct(){
    	$this->middleware('auth');

        
       if(config('app.env') == "production"){
            $this->moneywave = new Moneywave(null,"lv_SF453BCC6I6LWGAGT4GR",
                                            "lv_8GWM0RSY4QL3KS8NYOKYCDHWY5EZ1G",
                                            Environment::PRODUCTION);
        }else{
            $this->moneywave = new Moneywave(null,"ts_OII3YLXUT7CHDJ57A73N",
                                            "ts_T89VE2QZQVPT3JUSCCW87C17PBUEYK",
                                            Environment::STAGING);
        }
    }

    public function withdrawalView(){
        $bankService = $this->moneywave->createBanksService();
        $response = $bankService->send();

        $banks = $response->getData();

        $bank_details = Auth::user()->bankDetails()->get();

        if($bank_details->count() < 1){
            return redirect('payments/addaccount')->with('status','Add a bank account to withdraw!');
        }

    	return view('withdrawal',compact('bank_details','banks'));
    }

    public function addBankAccountView(){
        $bankService = $this->moneywave->createBanksService();
        $response = $bankService->send();
        $banks = [];

        if($response->isSuccessful()){
             $banks = $response->getData();
        }
       
        return view('addaccount',compact('banks'));
    }

    public function addBankAccount(Request $request){
        $accountExists = Auth::user()->bankDetails()
                                     ->where('account_no', $request->input('account-no'))
                                     ->count() > 0 ? true : false; 

        if(!$accountExists){
            $bankDetail = new BankDetail();
            $bankDetail->user_id = Auth::user()->id;
            $bankDetail->account_name = $request->input('account-name');

            $bankDetail->account_no = $request->input('account-no');
            $bankDetail->bank_code = $request->input('bank-code');
            $bankDetail->save();
        }else{
            return back()->with('error','Account number exists in records!');
        }

        return redirect('payments/withdraw')->with('status','Account added successfully!');
    }

    public function banks(){
        $result = Banks::allBanks();

    }

    public function withdraw(Request $request){
        $bankDetails = Auth::user()->bankDetails->find($request->account);

        if($request->amount <= Auth::user()->account_balance){
            $disburse = $this->moneywave->createDisburseService();
            $disburse->lock = 'emploi';
            $disburse->bankcode = $bankDetails->bank_code;
            $disburse->accountNumber = $bankDetails->account_no;
            $disburse->amount = $request->amount;
            $disburse->senderName = 'Emploi';
            $response = $disburse->send();

            if($response->isSuccessful()){
                DB::transaction(function()use($request,$response){
                    $user = Auth::user();
                    $user->account_balance -= $request->amount;
                    $user->save();

                    $payment = new Payment();
                    $payment->user_id = $user->id;
                    $payment->reciever_id = 0;
                    $payment->narration = "withdrawal from account";
                    $payment->reference_no = $response->getData()["data"]["uniquereference"];
                    $payment->amount = $request->amount;
                    $payment->save();

                    $payment_detail_1 = new PaymentDetail();
                    $payment_detail_1->payment_id = $payment->id;
                    $payment_detail_1->user_id = 0;
                    $payment_detail_1->amount = $payment->amount;
                    $payment_detail_1->save();

                    $payment_detail_2 = new PaymentDetail();
                    $payment_detail_2->payment_id = $payment->id;
                    $payment_detail_2->user_id = $user->id;
                    $payment_detail_2->amount = $payment->amount * -1;
                    $payment_detail_2->save();
                });
            }
            return $response->getRawResponse();
        }

        return ["message" => "Insufficient Balance", "status" =>"error"];

    }

}
