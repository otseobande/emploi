<?php
namespace Emploi\Http\Controllers\Payment;

use Emmanix2002\Moneywave\Enum\PaymentMedium;
use Emmanix2002\Moneywave\Enum\Environment;
use Emmanix2002\Moneywave\Enum\AuthorizationType;
use Emmanix2002\Moneywave\Exception\ValidationException;
use Emmanix2002\Moneywave\Moneywave;

use Emploi\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Emploi\AccountDetails;
use Emploi\{Payment,PaymentDetail};

class DepositController extends Controller
{
	private $merchantKey;
	private $apiKey;
	private $env; //ca

    public function __construct(){
    	$this->middleware('auth');
    	
    	if(config('app.env') == "production"){
    		$this->moneywave = new Moneywave(null,"lv_SF453BCC6I6LWGAGT4GR",
    										"lv_8GWM0RSY4QL3KS8NYOKYCDHWY5EZ1G",
    										Environment::PRODUCTION);
    	}else{
    		$this->moneywave = new Moneywave(null,"ts_OII3YLXUT7CHDJ57A73N",
    										"ts_T89VE2QZQVPT3JUSCCW87C17PBUEYK",
    										Environment::STAGING);
    	}
    	
    }

    public function addFundsView(Request $request){
    	return view('addfunds');
    }

    public function addFunds(Request $request){
   
		$cardToWallet = $this->moneywave->createCardToWalletService();
	    $cardToWallet->firstname = Auth::user()->name;
	    $cardToWallet->lastname = Auth::user()->name;
	    $cardToWallet->phonenumber = "+".(string)Auth::user()->phone_number;
	    $cardToWallet->email = Auth::user()->email;
	    $cardToWallet->card_no = $request->cardNo;
	    $cardToWallet->cvv = $request->cvv;
	    $cardToWallet->expiry_year = $request->expiryYear;
	    $cardToWallet->expiry_month = $request->expiryMonth;
	    $cardToWallet->pin = $request->pin;
	    $cardToWallet->charge_auth = "PIN";
	    $cardToWallet->amount = $request->amount;
	    $cardToWallet->fee = $request->amount * 0.05;
	    $cardToWallet->redirecturl = "http://Emploi.localhost.com/payments/addfunds";
	    $cardToWallet->medium = PaymentMedium::WEB;
	    $response = $cardToWallet->send();

	    return $response->getRawResponse();
		

    }

    public function status(Request $request){
    	dd($request);
    }

    public function validateCard(Request $request){
    	
    	$validateTransfer = $this->moneywave->createValidateCardTransferService();
	    $validateTransfer->transactionRef = $request->ref;
	    $validateTransfer->authType = AuthorizationType::OTP;
	    $validateTransfer->otp = $request->otp;
	    $response = $validateTransfer->send();

		if($response->isSuccessful()) {
		  	DB::transaction(function()use($request){
				$user = Auth::user();
				$user->account_balance += $request->amount;
				$user->save();

				$payment = new Payment();
				$payment->user_id = $user->id;
				$payment->reciever_id = 0;
				$payment->narration = "Deposit to account";
				$payment->reference_no = $request->ref;
				$payment->amount = $request->amount;
				$payment->save();

				$payment_detail_1 = new PaymentDetail();
				$payment_detail_1->payment_id = $payment->id;
				$payment_detail_1->user_id = $user->id;
				$payment_detail_1->amount = $payment->amount;
				$payment_detail_1->save();

				$payment_detail_2 = new PaymentDetail();
				$payment_detail_2->payment_id = $payment->id;
				$payment_detail_2->user_id = 0;
				$payment_detail_2->amount = $payment->amount * -1;
				$payment_detail_2->save();
			});
		  	
		}

		return $response->getRawResponse();
		   	
    }


}

