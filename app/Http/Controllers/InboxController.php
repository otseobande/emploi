<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Emploi\User;
use Carbon\Carbon;
use Emploi\{Message, Thread, Participant, MessageAttachment, Order};
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\{Auth, Input, Session, DB};

class InboxController extends Controller
{
    //
    public function __construct(){
    	$this->middleware('auth');
    }
    public function index(){
    	$users = User::all();
    	$threads = Thread::forUser(Auth::user()->id)->orderBy('updated_at','desc')->get();
 
    	return view('conversations',compact('threads','users'));
    }

     public function show($id){
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();

        $thread->markAsRead($userId);

        return view('messenger.show', compact('thread', 'users'));
    }

    public function create(){
        $users = User::where('id', '!=', Auth::id())->get();

        return view('messenger.create', compact('users'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(){
        $input = Input::all();

        $thread = Thread::create([
            'subject' => $input['subject'],
        ]);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $input['message'],
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }

        return redirect()->route('messages.show', $id);
    }

    public function conversation(Request $request, User $user, Thread $threadId){

    	$all_threads = Thread::all();
    	$thread = [];
    	$messages = [];
        $employment_orders = [];

        foreach(Auth::user()->gigs as $gig){
            foreach($gig->orders as $order){
                if($order->employer_id == $user->id){
                    array_push($employment_orders, $order);
                }
                
            }
        }
                                        
        $employer_orders = [];

        foreach(Auth::user()->orders as $order){
            if($order->gig->user->id == $user->id){
                array_push($employer_orders,$order);
            }
        }

    	foreach($all_threads as $thread){
    		$user1In = $thread->participants()->where('user_id',$user->id)->count() > 0;
    		$user2In = $thread->participants()->where('user_id',Auth::user()->id)->count() > 0;
    		if($user1In && $user2In){
    			$messages = $thread->messages()->get();
    			break;
    		}
    	}

    	return view('conversation',compact('user','messages','thread','employment_orders','employer_orders'));
    }

    public function sendMessage(Request $request, User $user){
        

    	DB::transaction(function()use($request,$user){

    		$threads = Thread::all();
    		$threadCreated = false;

    		foreach($threads as $thread){
	    		$user1In = $thread->participants()->where('user_id',$user->id)->count() > 0;
	    		$user2In = $thread->participants()->where('user_id',Auth::user()->id)->count() > 0;
	    		if($user1In && $user2In){
	    			$threadCreated = true;
	    			break;
	    		}
	    	}

    		if($threadCreated){
    			$thread = Thread::find($request->input('thread-id'));
		    	$message = new Message();
		    	$message->thread_id = $thread->id;
		    	$message->user_id = Auth::user()->id;
		    	$message->body =  $request->message;
		    	$message->save();
    		}else{
	    		$thread = new Thread();
		    	$thread->subject = 'conversation';
		    	$thread->save();

		    	$message = new Message();
		    	$message->thread_id = $thread->id;
		    	$message->user_id = Auth::user()->id;
		    	$message->body =  $request->message;
		    	$message->save();

		    	$thread->addParticipant([Auth::user()->id,$user->id]);
		    }

            if($request->file('attachment')){
                $attachmentPath = $request->file('attachment')
                                    ->store('attachments');

                $attachment = new MessageAttachment();
                $attachment->message_id = $message->id;
                $attachment->link = $attachmentPath;
                $attachment->filename = $request->file('attachment')->getClientOriginalName();
                $attachment->save();
            }
    	});

    	return back();
    }

    public function addMessageToThread(Request $request, User $user){
    	$thread = Thread::find($request->input('thread-id'));
    	$message = new Message();
    	$message->thread_id = $thread->id;
    	$message->user_id = Auth::user()->id;
    	$message->body =  $request->input('message');
    	$message->save();

    	return back();
    }	
}
