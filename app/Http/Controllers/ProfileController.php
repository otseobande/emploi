<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Storage};
use Emploi\User;
use Emploi\Gig;
use Emploi\UserSkill;


class ProfileController extends Controller
{
    //

    public function __construct(){
    	$this->middleware('auth');
    }
    
    public function changeProfilePicture(Request $request){
        $user = Auth::user();
       
        if($request->file('profileImg')){
            
            if($user->profile_image_link != "img/profile/user.png"){
                Storage::delete(Auth::user()->profile_image_link);
            }

            $link = $request->file('profileImg')->store('profile_pics');
            $user->profile_image_link = $link;
            $user->save();
        }

        return ['message' => 'profile image changed successfully.'];
    }

    public function index(Request $request, User $user){
     	$gigs = Auth::user()->gigs()->where('active',1)->get();
     	$own_profile = Auth::user()->id == $user->id;

    	return view('profile',compact('user','gigs', 'own_profile'));
    }

    public function editDescription(Request $request){
    	$user = Auth::user();
    	$user->description = $request->input('description');
    	$user->save();

    	return back();
    }

    public function addSkill(Request $request){
    	$skill = new UserSkill;
    	$skill->user_id = Auth::user()->id;
    	$skill->name = $request->input('skill-name');
    	$skill->experience_level = $request->input('skill-experience-level');
    	$skill->save();

    	return back();
    }

    public function removeSkill(UserSkill $skill){
        $skill->delete();

        return ["message"=> "skill removed successfully"];
    }
}
