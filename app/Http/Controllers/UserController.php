<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\User;

class UserController extends Controller
{
    //
    public function __contruct(){
        $this->middleware('auth');
    }


    private function isUserLoggedIn($user){
        if (Auth::user()->username != $user->username){
            return false;
        }
        return true;
    }

    public function uploadAvatar(Request $request){
    	$avatarPath = $request->file('avatar')->store('img/profile');

    	User::where('id',Auth::id())->update(['avatar' => $avatarPath]);

    	return back();
    }

    public function manageRequests(Request $request, User $user){

    }

    public function manageGigs(Request $request, User $user){
        if($this->isUserLoggedIn($user)){
            return redirect()->route('dashboard');
        }

        return redirect()->route('home');
        
    }

    public function manageOrders(Request $request, User $user){
        if($this->isUserLoggedIn($user)){
            return redirect()->route('dashboard');
        }
    }

    public function teamAccountView(Request $request){
        return view('team-account');
    }

}
