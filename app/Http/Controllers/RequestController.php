<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\Category;
use Emploi\{EmployerRequest, RequestAttachment};
use Emploi\Offer;

class RequestController extends Controller
{
    //
	public function __construct(){
		$this->middleware('auth'); 
	}

    public function newRequestView(){
    	$categories = Category::all();
    	$categories_arr = [];
   
    	foreach($categories as $category){
    		$temp_arr = [];
    		$temp_arr['name'] = $category->name;
    		$temp_arr['id'] = $category->id;
    		$temp_arr['subcategories'] = [];
    		

    		foreach($category->subcategories()->get() as $subcategory){
    			$temp_subcategory_arr = [];
    			$temp_subcategory_arr['name'] = $subcategory->name;
    			$temp_subcategory_arr['id'] = $subcategory->id;
    			array_push($temp_arr['subcategories'],$temp_subcategory_arr);
    		}
    		array_push($categories_arr,$temp_arr);
    	}

    	$json_categories = json_encode($categories_arr);
    	return view('newrequest',compact('categories','json_categories'));
    }

    public function postNewRequest(Request $request){
    	$employer_request = new EmployerRequest();
    	$employer_request->user_id = Auth::user()->id; 
    	$employer_request->description = $request->input('description');
    	$employer_request->category_id = $request->input('category');
    	$employer_request->sub_category_id = $request->input('sub-category');
    	$employer_request->delivery_period = $request->input('delivery-period');
    	$employer_request->budget = $request->input('budget');
    	$employer_request->active = 1;
    	$employer_request->save();

        if($request->file('attachment')){
            $attachment = new RequestAttachment();
            $attachment->employer_request_id = $employer_request->id;
            $attachment->link = $request->file('attachment')->store('attachments');
            $attachment->save();
        }

        
    	return redirect('requests/manage'); 
    }

    public function manageRequests(){
    	$active_requests = Auth::user()->requests()
                                       ->where('active',1)
                                       ->where('accepted',0)
                                       ->paginate(15);

    	$suspended_requests = Auth::user()->requests()->where('active',0)->paginate(15);
        
        $accepted_requests = Auth::user()->requests()->where('accepted',1)->paginate(15);
    	return view('manage-requests',compact('active_requests','suspended_requests','accepted_requests'));
    }

    public function all(){
    	$requests = EmployerRequest::where('active',1)
                                    ->where('accepted',0)
                                    ->where('user_id', '!=', Auth::user()->id)
                                    ->orderBy('created_at','desc')
                                    ->paginate(20);

    	return view('employer-requests',compact('requests'));
    }

    public function suspend(EmployerRequest $request){
    	$request->active = 0;
    	$request->save();
    	return ['message'=>'Employer Request Suspended'];
    }

    public function activate(EmployerRequest $request){
    	$request->active = 1;
    	$request->save();
    	return ['message'=>'Employer Request Activated'];
    }

    public function viewOffers(EmployerRequest $request){
    	return view('view-offers',compact('request'));
    }

    public function makeOfferView(EmployerRequest $request){
    	return view('make-offer',compact('request'));
    }

    public function makeOffer(Request $request){
    	$offer = new Offer();
    	$offer->user_id = Auth::user()->id;
    	$offer->employer_request_id = $request->input('request-id');
    	$offer->gig_id = $request->input('gig-id');
    	$offer->details = $request->input('details');
    	$offer->price = $request->input('amount');
    	$offer->delivery_period = $request->input('delivery-period');
    	$offer->save();

    	return redirect('requests/all');
    }

    public function cancelOffer($request_id){
    	Auth::user()->offers()->where('employer_request_id',$request_id)->delete();
    	
    	return ['message' => 'Offer cancelled'];
    }
}
