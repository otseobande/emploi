<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;

use Emploi\Gig;
use Emploi\User;

class SearchController extends Controller
{
    //
    public function index(){

    }

    public function search(Request $request){
    	$query = $request->q;
    	
    	if($request->has('q')){
    		$query = $request->input('q');
    	}
        $users = User::where('name','like','%'.$query.'%')
                      ->orWhere('username','like','%'.$query.'%')
                      ->get();
        $gigs = Gig::where('title','like','%'.$query.'%') 
                    ->orWhere('description','like','%'.$query.'%')
                    ->get();  	
    	return view('search',compact('users','gigs','query'));
    }
}
