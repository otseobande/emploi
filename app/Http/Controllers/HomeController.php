<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\User;
use Emploi\Gig;
use Emploi\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gigs = Gig::orderBy('created_at', 'desc')->paginate(60);
        $categories = Category::all();

        if (Auth::guest()){
            return view('guesthome',compact('categories'));
        }else{
            return view('home', compact('gigs','categories'));
        }
    }
}
