<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Emploi\User;

class SettingsController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){
    	return view('settings');
    }

    public function validator(array $data){
		return Validator::make($data,[
    		'old_password' => 'required|string|min:6',
    		'new_password' => 'required|string|min:6|confirmed'
    	]);

	}

    public function changePassword(Request $request){
    	
    	$this->validator($request->all())->validate();
    	if(Hash::check($request->old_password,Auth::user()->password)){
    		$new_password_hashed = Hash::make($request->new_password);

    		$user = User::where('id',Auth::user()->id)->update(['password' => $new_password_hashed]);
    		
    		return back()->with('success','password changed successfully');
    	}else{
    		return redirect()->back()->withErrors([
    			'message' => 'old password incorrect'
    		]);
    	}

    	return redirect('settings');
    }
}
