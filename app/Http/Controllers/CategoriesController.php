<?php 

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Emploi\Category;
use Emploi\SubCategory;
use Carbon\Carbon;

class CategoriesController extends Controller
{
    //

    public function createCategories(){
        $categories = [['name' => 'Graphics & Design', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Marketing', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Writing & Translation', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Video & Animation', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Music & Audio', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Fashion & Art', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Programming & Tech',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                        ['name' => 'Consulting',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                       ['name' => 'Business', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                        ['name' => 'Entertainment, Events & Environment', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                        ['name' => 'Fun & Lifestyle', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],

                        ['name' => 'Others', 
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                       ],


                    ];

        $entry = Category::insert($categories);
        
        return $entry ? 'Successfully updated categories' : 'Error Updating categories';
    }

    public function createSubCategories(){

        $categories = Category::all();
        $value = null;
        foreach ($categories as $category) {
            switch ($category->name) {
                case 'Graphics & Design':
                    $this->insertSubCategories($category->id,[
                            'Logo Design',
                            'Flyer Design',
                            'Interior design',
                            'Architectural design',
                            'Fashion design',
                            'Photo editting',
                            'Web Design',
                            'Cartoons & Caricature',
                            'Tshirt Design',
                            'Banner Design',
                            'Others'
                        ]);
                    break;
                case 'Marketing':
                    $this->insertSubCategories($category->id,[
                            'Digital Marketing',
                            'Branding',
                            'Advertising',
                            'Content Marketing',
                            'Product Marketing',
                            'Video and Mobile Marketing',
                            'Social Media Marketing',
                            'Search Engine Optimization',
                            'Traffic Increase',
                            'Others'
                        ]);
                    break;
                case 'Writing & Translation':
                    $this->insertSubCategories($category->id,[
                            'Resumes & Cover letters/CV writing',
                            'Proof reading & Editing',
                            'Translation',
                            'Creative Writing',
                            'Feasiblity Report',
                            'Business Copy Writing',
                            'Research Writing',
                            'Articles & Blog Posts',
                            'Press Releases',
                            'Transcription',
                            'Data Entry/Typing',
                            'Others'
                        ]);
                    break;
                case 'Video & Animation':
                    $this->insertSubCategories($category->id,[
                            'Cinematography',
                            'Explainer Videos',
                            'Promotional & Brand Videos',
                            'Animated Characters',
                            'Editing & Post Production',
                            'Intros & animated logos',
                            'Commercials',
                            'Others'
                        ]);
                    break;
                case 'Music & Audio':
                    $this->insertSubCategories($category->id,[
                            'Voice Over',
                            'Mixing & Mastering',
                            'Jingles',
                            'Song writing',
                            'Music Production',
                            'Music Promotion',
                            'Others',
                        ]);
                    break;
                case 'Fashion & Art':
                    $this->insertSubCategories($category->id,[
                            'Shoe & Bag Making',
                            'Beauty and Makeup',
                            'Bead & Hat Making',
                            'sketch & painting',
                            'Stylist',
                            'Wig Making',
                            'Photography',
                            'Drawing & Paint work',
                            'Modelling',
                            'Others',

                        ]);
                    break;
                case 'Programming & Tech':
                    $this->insertSubCategories($category->id,[
                            'Web Development',
                            'Mobile apps',
                            'Software testing',
                            'Software engineering',
                            'Game Development',
                            'Development tools',
                            'GIS',
                            'Databases',
                            'Network and security',
                            'Others'
                        ]);
                    break;
                case 'Consulting':
                    $this->insertSubCategories($category->id,[
                            'Legal Consulting',
                            'Financial Consulting',
                            'Entertainment Consulting',
                            'Telecommunication Consulting',
                            'Business Consulting',
                            'Agricultural Consulting',
                            'Medical Consulting',
                            'Others'
                    ]);
                    break;
                case 'Business':
                    $this->insertSubCategories($category->id,[
                            'Busines law',
                            'Market Research',
                            'Business Plan',
                            'Business communication',
                            'Data & analytics',
                            'Logistics',
                            'Real Estate',
                            'Others'
                    ]);
                    break;
                case 'Entertainment, Events & Environment':
                    $this->insertSubCategories($category->id,[
                            'Live Performance',
                            'Ushering',
                            'Decoration',
                            'Laundry',
                            'Cleaning/Clearing',
                            'Catering Services',
                            'Others'
                    ]);
                    break;

                case 'Fun & Lifestyle':
                    $this->insertSubCategories($category->id,[
                            'Online Lessons',
                            'Relationship Advice',
                            'Hookup services',
                            'Parenting advice',
                            'Leadership',
                            'Religion and spirituality',
                            'Travel',
                            'Pet care and training',
                            'Seminars and talkshows',
                            'Health, Nutrition & Fitness',
                            'Gaming',
                            'Pranks',                            
                            'Others'
                    ]);
                    break;

                case 'Others':
                    $this->insertSubCategories($category->id,[                         
                            'Others'
                    ]);
                    break;
            }
        }

        return 'Successfully created subcategories';
    }

    private function insertSubCategories($id, $subcategories){
        $category = Category::find($id);

        $subCategoryList = [];

        foreach($subcategories as $subcategory){
            $item = new SubCategory(['name' => $subcategory]);
            array_push($subCategoryList, $item);
        }

        $subCategories = $category->subcategories()->saveMany($subCategoryList);

        return $subCategoryList;
    }

    public function searchView(Category $category){
        return view('categories',compact('category'));
    }
}
