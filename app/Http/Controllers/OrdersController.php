<?php

namespace Emploi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Emploi\{Order, Offer,Gig, Delivery, CancelRequest, ModificationRequest};
use Carbon\Carbon;

class OrdersController extends Controller
{
    //
    public function manageSales(){
    	// $user_orders = Order::employees()->where('id',Auth::user()->id);

    	// $active_orders = $user_orders->where('completed',0)->get();
    	return view('manage-sales');
    }

    public function acceptOffer(Offer $offer){
    	$offer->accepted = 1;
    	$offer->save();

    	$order = new Order();
    	$order->employer_id = Auth::user()->id;
    	$order->gig_id = $offer->gig_id;
    	$order->price = $offer->price;
    	$order->delivery_date = Carbon::now()->addDays($offer->delivery_period);
    	$order->save();

    	return ["message" => "Offer accepted"]; 
    }

    public function newOrder(){

    }

    public function viewGig(Gig $gig){
        return view('view-gig', compact('gig'));
    }

    public function manageOrders(){
        $active_orders = Auth::user()->orders()
                                     ->where('completed',0)
                                     ->where('cancelled',0)
                                     ->orderBy('id','desc')
                                     ->get();

        $delivered_orders = Auth::user()->orders()
                                     ->where('delivered',1)
                                     ->orderBy('id','desc')
                                     ->get();

        $cancelled_orders = Auth::user()->orders()
                                     ->where('cancelled',1)
                                     ->orderBy('id','desc')
                                     ->get();

        $completed_orders = Auth::user()->orders()
                                     ->where('completed',1)
                                     ->orderBy('id','desc')
                                     ->get();

        return view('manage-orders',compact("active_orders","cancelled_orders","completed_orders","delivered_orders"));
    }

    public function orderGig(Gig $gig){
        $order = new Order();
        $order->employer_id = Auth::user()->id;
        $order->gig_id = $gig->id;
        $order->price = $gig->price;
        $order->delivery_date = Carbon::now()->addDays($gig->delivery_period);
        $order->save();

        $user = Auth::user();
        $user->account_balance -= $gig->price;
        $user->save();

        return ["message" => "Gig Ordered"];
    }

    public function deliverOrder(Request $request, Order $order){
        $order->delivered = 1;
        $order->save();

        $attachmentPath = $request->file('delivery')
                                    ->store('deliveries');
        $delivery = new Delivery();
        $delivery->order_id = $order->id;
        $delivery->description = $request->description;
        $delivery->attachment_link = $attachmentPath;
        $delivery->save();

        return ["message" => "Order marked as delivered"];
    }

    public function cancelOrder(Order $order){
        $cancelRequest = new CancelRequest();
        $cancelRequest->order_id = $order->id;
        $cancelRequest->user_id = Auth::user()->id;
        $cancelRequest->save();
    }

    public function acceptCancellation(Order $order){
        $cancelRequest = CancelRequest::where('order_id',$order->id)->get();
        $cancelRequest->accepted = 1;
        $cancelRequest->save();

        return ['message' => 'Order cancelled successfully'];
    }

    public function requestModification(Request $request,Order $order){
        $order->modification_requested = 1;
        $order->delivered = 0;
        $order->save();

        $modification_request = new ModificationRequest();
        $modification_request->order_id = $order->id;
        $modification_request->reason = $request->modificationReason;
        $modification_request->save();

        return ['message' => 'Modification requested'];
    }
}
