<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class CustomOffer extends Model
{
    public function employer(){
    	return $this->belongsTo('Emploi/User', 'employer_id');
    }

    public function employee(){
    	return $this->belongsTo('Emploi/User');
    }
}

