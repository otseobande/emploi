<?php

namespace Emploi\Listeners;

use Emploi\Events\DepositMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDepositSuccessfulNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DepositMade  $event
     * @return void
     */
    public function handle(DepositMade $event)
    {
        //
    }
}
