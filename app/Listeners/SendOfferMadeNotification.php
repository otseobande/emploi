<?php

namespace Emploi\Listeners;

use Emploi\Events\OfferMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOfferMadeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OfferMade  $event
     * @return void
     */
    public function handle(OfferMade $event)
    {
        //
    }
}
