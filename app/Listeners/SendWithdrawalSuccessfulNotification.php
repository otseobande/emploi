<?php

namespace Emploi\Listeners;

use Emploi\Events\WithdrawalMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWithdrawalSuccessfulNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WithdrawalMade  $event
     * @return void
     */
    public function handle(WithdrawalMade $event)
    {
        //
    }
}
