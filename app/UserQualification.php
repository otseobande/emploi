<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class UserQualification extends Model
{
    //
    public function user(){
    	return $this->belongsTo('Emploi\User');
    }
}
