<?php

namespace Emploi\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
       'Illuminate\Auth\Events\Registered' => [
            'Emploi\Listeners\SendWelcomeMessage',
        ],

        'Emploi\Events\OrderMade' => [
            'Emploi\Listeners\SendOrderMadeNotification',
        ],

        'Emploi\Events\MessageRecieved'=> [
            'Emploi\Listeners\SendMessageRecievedNotification'
        ],

        'Emploi\Events\OfferMade' => [
            'Emploi\Listeners\SendOfferMadeNotification'
        ],

        'Emploi\Events\DepositMade' => [
            'Emploi\Listeners\SendDepositSuccessfulNotification'
        ],

        'Emploi\Events\WithdrawalMade' => [
            'Emploi\Listeners\SendWithdrawalSuccessfulNotification'
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
