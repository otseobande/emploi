<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    public function sender(){
    	return $this->belongsTo('Emploi\User','sender_id');
    }

    public function reciever(){
    	return $this->belongsTo('Emploi\User','reciever_id');
    }
}
