<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Models\Thread as VendorThread;

class Thread extends VendorThread
{
	
	
    public function messages()
    {
        return $this->hasMany('Emploi\Message', 'thread_id', 'id');
    }
}
