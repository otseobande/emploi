<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Gig extends Model
{
    protected $fillable = ['id','category_id','description','price','image'];

    public function user(){
    	return $this->belongsTo('Emploi\User');
    }

    public function images(){
    	return $this->hasMany('Emploi\GigImage');
    }
    
    public function category(){
    	return $this->belongsTo('Emploi\Category');
    }

    public function subcategory(){
    	return $this->belongsTo('Emploi\SubCategory');
    }

    public function orders(){
        return $this->hasMany('Emploi\Order');
    }
}
