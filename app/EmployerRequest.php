<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class EmployerRequest extends Model
{
    //

    public function offers(){
    	return $this->hasMany('Emploi\Offer');
    }

    public function user(){
    	return $this->belongsTo('Emploi\User');
    }

    public function attachment(){
    	return $this->hasOne('Emploi\RequestAttachment');
    }
}
