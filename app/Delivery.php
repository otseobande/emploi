<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public function order(){
    	return $this->belongsTo('Emploi\Order');
    }
}
