<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class MessageAttachment extends Model
{
    public function message(){
    	return $this->belongsTo('Emploi\Message');
    }
}
