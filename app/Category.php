<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    public function subcategories(){
    	return $this->hasMany('Emploi\SubCategory');
    }

    public function gigs(){
    	return $this->hasMany('Emploi\Gig');
    }
}
