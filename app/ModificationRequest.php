<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class ModificationRequest extends Model
{
    public function order(){
    	return $this->hasOne('Emploi\Order');
    }
}
