<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Models\Message as VendorMessage;

class Message extends VendorMessage
{
	protected $touches = ['thread'];
	
    public function attachments(){
    	return $this->hasMany('Emploi\MessageAttachment');
    }
}
