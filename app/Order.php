<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function employer(){
    	return $this->belongsTo('Emploi\User','employer_id');
    }

    public function gig(){
    	return $this->belongsTo('Emploi\Gig');
    }

    public function employee(){
    	return $this->hasManyThrough('Emploi\User','Emploi\Gig');
    }

    public function deliveries(){
        return $this->hasMany('Emploi\Delivery');
    }

    public function modification(){
        return $this->hasMany('Emploi\ModificationRequest');
    }
}
