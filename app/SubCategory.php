<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //
    protected $fillable = ['name'];
    public function category(){
    	return $this->hasOne('Emploi\Category');
    }

    public function gigs(){
    	return $this->hasMany('Emploi\Gig');
    }
}
