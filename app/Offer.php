<?php

namespace Emploi;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //
    public function request(){
    	return $this->belongsTo('Emploi\EmployerRequest');
    }

    public function user(){
    	return $this->belongsTo('Emploi\User');
    }
}
