@extends('layouts.app')



@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="content-box">
				
				<img class="profile_img" src="{{ asset('storage/'.Auth::user()->profile_image_link) }}">
					
				<br>
				<div class="row">
					<div class="col-md-9">
						<p><strong>Positive Rating</strong></p>
					</div>
					<div class="col-md-3">
						<p><strong>0%</strong></p>
					</div>
				</div><hr>
				{{-- <div class="row">
					<div class="col-md-9">
						<p><strong>Response Time</strong></p>
					</div>
					<div class="col-md-3">
						<p><strong>12Hrs</strong></p>
					</div>
				</div><hr> --}}
				<div class="row">
					<div class="col-md-9">
						<strong>Response Rate</strong>
					</div>
					<div class="col-md-3">
						<strong>100%</strong>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-md-9">
						<strong>Orders Completed</strong>
					</div>
					<div class="col-md-3">
						<strong>100%</strong>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-md-9">
						<strong>Delivered on Time</strong>
					</div>
					<div class="col-md-3">
						<strong>100%</strong>
					</div>
				</div><hr>
			</div>
		</div>

		<div class="col-md-9">
			<div class="content-box" id="details-board">
				<div class="row">
					<div class="col-md-3 right-bordered">
						<h5>Jobs in Queue</h5>
						<p>0</p>
						
					</div>
					<div class="col-md-3 right-bordered">
						<h5>Jobs in Queue</h5>
						<p>0</p>
						
					</div>
					<div class="col-md-3 right-bordered">
						<h5>Balance</h5>
						<p>0</p>
						
					</div>
					<div class="col-md-3">
						<h5>Total Earnings</h5>
						<p>0</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection