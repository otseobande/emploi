@extends('layouts.app')

@section('style')
<style>
	.logo{
		margin-top: 10px;
		text-align: center;
	}
	.logo > img{
		margin: 2px;
	}
</style>
@endsection
@section('content')
<div class="container">
	<div class="row add-funds-box">
		<h3>Add funds</h3>
		<el-card class="">
			<div v-if="!payment.status.amountValid">
				<form>
					<div :class="{'form-group' : true,'has-error': errors.has('amount')}">
						<label>Deposit amount:</label>
					  	<div class="input-group">
							<span class="input-group-addon">&#8358;</span>
							<input autofocus v-validate="'required|min_value:1000|numeric'" name="amount" type="number" v-model="payment.details.amount" type="text" class="form-control" placeholder="Enter amount"> 
						</div>
						<span v-show="errors.has('amount')" class="help-block">
	                        <strong>@{{ errors.first('amount') }}</strong>
	                    </span><br>
	                </div>
				  <button :disabled="errors.any() || !payment.details.amount" class="btn btn-primary" @click.prevent="validateAmount">Continue</button>
				</form>
			</div>
			<div v-else>
				<div v-if="!payment.status.sendingOtp && !payment.status.validatingCard">
					<div v-if="!payment.status.cardValid" @keyup.enter="addFunds">
				     <el-alert
					    :title="payment.error"
					    type="error"
					    v-if="payment.error">
					  </el-alert>

					<form>
					  <div :class="{'form-group':true,'has-error': errors.has('cardNo')}">
					  	<label for="cardNo">Card number:</label>
					    <input autofocus v-validate="'required|numeric'" type="number" v-model="payment.details.cardNo" name="cardNo" id="cardNo" placeholder="Enter card number" class="form-control">
					    <span v-show="errors.has('cardNo')" class="help-block">
	                        <strong>@{{ errors.first('cardNo') }}</strong>
	                    </span>
					  </div>
					  <div class="row">
					  		<div class="col-md-6">
					  			<div :class="{'form-group':true,'has-error': errors.has('month')}">
					  				<label for="month">Expiry month:</label>
					  				<input v-validate="'required|numeric|max:2|max_value:12'" type="number" v-model="payment.details.expiryMonth" name="month" id="month" placeholder="MM"
					  				class="form-control">
					  				<span v-show="errors.has('month')" class="help-block">
				                        <strong>@{{ errors.first('month') }}</strong>
				                    </span>
					  			</div>
					  			
					  		</div>
					  		<div class="col-md-6">
					  			<div :class="{'form-group':true, 'has-error': errors.has('year')}">
					  				<label for="year">Expiry year:</label>
					  				<input v-validate="'required|numeric|max:2'" type="number" name="year" id="year" 
					  				v-model="payment.details.expiryYear" placeholder="YY" class="form-control">
					  				<span v-show="errors.has('year')" class="help-block">
				                        <strong>@{{ errors.first('year') }}</strong>
				                    </span>
					  			</div>
					  			
					  		</div>
					  </div>
					   <div class="row">
					  		<div class="col-md-6">
					  			<div :class="{'form-group':true, 'has-error':errors.has('cvv')}">
					  				<label for="cvv">CVV2:</label>
					  				<input v-validate="'required|numeric'" v-model="payment.details.cvv" type="number" id="cvv" name="cvv"
					  				class="form-control">
					  				<span v-show="errors.has('cvv')" class="help-block">
				                        <strong>@{{ errors.first('cvv') }}</strong>
				                    </span>
					  			</div>
					  	
					  		</div>
					  		<div class="col-md-6">
					  			<div :class="{'form-group':true, 'has-error': errors.has('pin')}">
					  				<label for="pin">Pin:</label>
					  				<input v-validate="'required'" type="password" name="pin" id="pin" v-model="payment.details.pin" 
					  				class="form-control">
					  				<span v-show="errors.has('pin')" class="help-block">
				                        <strong>@{{ errors.first('pin') }}</strong>
				                    </span>
					  			</div>
					  		</div>
					  </div>
					  <button :disabled="errors.any()" class="btn btn-primary" type="button" @click.prevent="addFunds">Continue</button>
					  <button class="btn btn-default" type="button" @click.prevent="payment.status.amountValid = false">Back</button>
					</form>
					</div>
					<div v-else>
						<div v-if="!payment.status.otpValid">
							<div v-if="payment.error" class="alert alert-danger">
				                <p>@{{payment.error}}</p>
				            </div>
							<h5>@{{payment.response.data.transfer.flutterChargeResponseMessage}}</h5>
							<form>

								<div :class="{'form-group':true, 'has-error' : errors.has('otp')}">
									<label for="otp">OTP:</label>
									<input autofocus v-validate="'required'" v-model="payment.details.otp" name="otp" id="otp" placeholder="Enter otp" class="form-control">
									<span v-show="errors.has('otp')" class="help-block">
				                        <strong>@{{ errors.first('otp') }}</strong>
				                    </span>
								</div>
								
								<button :disabled="errors.any() || !payment.details.otp" class="btn btn-primary" type="button" @click.prevent="validateCard">Submit</button>
								<button class="btn btn-default" type="button" @click.prevent="payment.status.cardValid = false;">Back</button>
							</form>
							<a v-if="payment.error" @click="validateCard">Click to resend otp</a>
						</div>
						<div v-else style="text-align: center">
							<h3>Deposit Successful</h3>
							<a href="{{url("payments/summary")}}">Click here to payment summary</a>
						</div>
					</div>
				</div>
				<div v-else style="text-align: center;">
					<h5>Please wait...</h5>
					<img width="50" src="{{asset('img/rolling.gif')}}">
				</div>
			</div>
		</el-card>
		<div class="logo">
			<img width="30" src="{{asset('img/mastercard.png')}}">
			<img width="60" src="{{asset('img/visa.png')}}">
			<img width="60" src="{{asset('img/verve.png')}}">
			<p style="font-size: 14px;">Emploi&copy; does not store your card details on our servers.</p>
		</div>
	</div>
</div>
@endsection