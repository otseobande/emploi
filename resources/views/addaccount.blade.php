@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row add-funds-box">
			@if (session('status'))
				<div class="alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">
					&times;
					</button>
					{{session('status')}}
				</div>
			@endif
			@if (session('error'))
			     <div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">
					&times;
					</button>
					{{session('status')}}
				</div>
			@endif
			<br>
			<el-card>
				<form action="{{ url('payments/addaccount')}}" method="POST">
					{{csrf_field()}}
					<div>
						<label>Account Name:</label>
						<input class="form-control" name="account-name">
					</div>
					<div>
						<label>Account Number:</label>
						<input class="form-control" name="account-no">
					</div>
					<div>
						<label>Bank:</label>
						<select class="form-control" name="bank-code">
							@foreach($banks as $code => $name)
								<option value="{{$code}}">{{$name}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<button class="btn btn-primary">Add account</button>
				</form>
			</el-card>
		</div>
	</div>
@endsection