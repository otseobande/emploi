@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Post a request to Employees</h3>
			<div class="col-md-10">
				<el-card>
					<form class="form-horizontal" role="form" action="{{url('requests/new')}}" method="POST" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Description</label>
								<div class="col-sm-10">
									{{-- <textarea class="form-control gig-title hidden" maxlength="800" id="description" name="description" required></textarea> --}}
									<el-input
									  id="description"
									  name="description"
									  type="textarea"
									  :autosize="{ minRows: 2, maxRows: 4}"
									  
									  maxlength="150"
									  required
									  >
									</el-input>
								</div>
							</div>
							<div class="form-group">
								<label for="category" class="col-sm-2 control-label">Category</label>
								<div class="col-sm-10">
									<select class="form-control" id="category" name="category" v-model="categorySelected" required>
										<option value="">Select Category</option>
										<option v-for="i in categories" :key="i.id" :value="i.id">
											@{{i.name}}
										</option>
									</select>
								</div>
							</div>
							<div v-if="categorySelected">
								<div class="form-group">
									<label for="sub-category" class="col-sm-2 control-label">Sub Category</label>
									<div class="col-sm-10">
										<select class="form-control" id="sub-category" name="sub-category" required>
											<option value="">Select subcategory</option>
											<option v-for="i in subcategories" :key="i.id" :value="i.id">
												@{{i.name}}
											</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="delivery-period" class="col-sm-2 control-label">Delivery Period</label>
								<div class="col-sm-10">
									<select class="form-control" id="delivery-period" name="delivery-period">
										<option value="1">1 day</option>
										@for($i=2; $i<=29; $i++)
											<option value="{{$i}}">{{$i}} days</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="budget" class="col-sm-2 control-label">Budget Price</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">&#8358;</span>
										<input type="number" name="budget" id="budget" min="1000" class="form-control" placeholder="Budget" required>
									</div>
									<div style="font-size: 12px">from &#8358;1000</div>
								</div>
							</div>
							<div class="form-group">
								<label for="budget" class="col-sm-2 control-label">Attachement</label>
								<div class="col-sm-10">
									
										<input type="file" name="attachment" id="attachement" class="form-control">
							
									
								</div>
							</div>
							<button class="btn btn-primary pull-right" type="submit">Post</button>
					</form>
				</el-card>
			</div>

		</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script>
	app.categories = {!! $json_categories !!}
</script>
@endsection