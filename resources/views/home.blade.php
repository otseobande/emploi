@extends('layouts.app')



@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="card welcome-box" style="padding: 10px;">
					<h4><strong>Hi, {{Auth::user()->name}}</strong></h4>
					<p style="text-align: left;">Get offers from employees for your project</p>
					<br>
					<a type="button" class="btn btn-success btn-block" href="{{ url('request/new')}}">Post a request</a>
					
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel">
					<div class="carousel slide" id="myCarousel" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="myCarousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<div class="item active">
								<img src="{{ asset('img/images.jpg') }}">
							</div>

							<div class="item">
								<img src="{{ asset('img/programming.png')}}">
							</div>

							<div class="item">
								<img src="{{ asset('img/vo.jpg')}}">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<h3><strong>Recent Gigs</strong></h3>
			</div>
		</div>
		<div style="margin-bottom: 30px;">
			@foreach($gigs->chunk(6) as $chunk)
			<div class="row">
				@foreach($chunk as $gig)
					<div class="col-sm-4 col-md-2">	
						<div class="card gig-block">
							<div class="gig-block-image">
								<a href="{{url('orders/gig/'.$gig->id)}}">
									<img src="{{ asset('storage/'.$gig->image)}}">
								</a>
							</div>
							<div class="gig-block-body">
								<div class="row">
									<div class="col-xs-3" style="padding-right: 0px;">
										<img class="img-circle img-responsive user-img" src="{{ asset('img/profile/user.png')}}">
									</div>
									<div class="col-xs-9">
										 <span class="user"><a href="{{url('profile/'.$gig->user->username)}}">{{$gig->user->username}}</a></span><br>
										{{--  <el-rate 
										 	style="display: inline;"
											:value="{{$gig->user->ratings()}}"
											style="margin: auto; width: 121px;"
										    :colors="['#99A9BF', '#F7BA2A', '#FF9900']" 
										    disabled>
										 </el-rate> --}}
									</div>
								</div>
								<div class="title">
									<a href="{{url('orders/gig/'.$gig->id)}}"><b>{{$gig->title}}</b></a>
								</div>
							</div>
							<div class="gig-block-footer">
								<span class="" style="font-size: 12px">From: &#8358;{{number_format($gig->price)}}</span>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		@endforeach
		{{$gigs->links()}}
	</div>
@endsection
