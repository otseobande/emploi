@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3>Employer Requests</h3>
			<div class="col-md-12">
				<el-card style="overflow: auto;	">
					@if($requests->count() > 0)
					<table class="table table-striped table-responsive">
						<thead>
							<th>Created</th>
							<th>Employer</th>
							<th>Request</th>
							<th>Budget</th>
							<th>Delivery Period</th>
							<th>Offers</th>
							<th>action</th>
						</thead>
						<tbody>
							@foreach($requests as $request)
							<tr>
								<td>{{date("M jS, Y", strtotime($request->created_at))}}</td>
								<td>
									<img class="img-circle" style="width: 20px;" src="{{asset($request->user->profile_image_link)}}">
									<a href="{{url("/profile/".$request->user->username)}}">{{$request->user->username}}</a>
								</td>
								<td>
									{{$request->description}}<br>
									@if($request->attachment)
										<a href="{{ url('storage/attachments'.$request->attachment->link)}}">Download Attachment</a>
									@endif
								</td>
								<td>&#8358;{{number_format($request->budget)}}</td>
								<td>{{$request->delivery_period}} day(s)</td>
								<td>{{$request->offers()->count()}}</td>
								<td>
									@if($request->user_id != Auth::user()->id)
										@if($request->offers()->where('user_id',Auth::user()->id)->count() < 1)
										<a class="btn btn-sm btn-primary" href="{{ url('requests/'.$request->id.'/offer/')}}">Make Offer</a>
										@else
											<button href="" class="btn btn-sm btn-danger" @click="cancelOffer({{$request->id}})">Cancel Offer</button>
										@endif
									@else
										<a href="{{url('request/manage/'.$request->id)}}" class="btn btn-sm btn-default">View Offers</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{$requests->links()}}
					@else
						<div style="text-align: center">
							<p>No records found</p>
						</div>
					@endif
				</el-card>
				
			</div>
		</div>
	</div>
@endsection