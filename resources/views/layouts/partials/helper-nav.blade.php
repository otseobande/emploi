<div class="nav helper-nav hidden-sm hidden-xs">
	<div class="container">
		<nav class="row">
			<ul>
				@foreach($categories as $category)
						<li class=""><a href="#">{{ $category->name}}</a></li>
				@endforeach
			</ul>
		</nav>
	</div>
</div>