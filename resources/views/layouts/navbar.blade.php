<nav class="navbar navbar-inverse navbar-static-top" id="main-nav">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" >
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <!-- Branding Image -->
            <a id="brand-name" class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('img/logo_name.png')}}" width="100">
            </a>

        </div>

        @if(!Auth::guest())
        <div>
            <form class="navbar-form navbar-left" role="search" action="{{route('search')}}">
                <div class="input-group">
                    <input type="text" name="q" id="jobsearch" placeholder="Search jobs or people" class="form-control jobsearch">
                    <span class="input-group-btn hidden-xs">
                       <button class="btn btn-default" style="height: 35px; background-color: #f2f2f2;" type="button" @click="toggleShowCategories">
                          <i class="fa fa-bars"></i>
                       </button>
                    </span>
                </div>   
            </form>
            <div class="card categories-search hidden-xs" v-if="showCategories">
                <div class="container">
                        @foreach($categories->chunk(3) as $chunk)
                            <div class="row"> 
                                @foreach($chunk as $category)
                                    <div class="col-md-4" style="width: 16%;">
                                        <a href="{{url('categories/'.$category->id)}}"><h4>{{$category->name}}</h4></a>
                                        <ul style="padding: 0px">
                                        @foreach($category->subcategories()->get() as $subcategory)
                                            <li>
                                                @if($loop->iteration == 5)
                                                    ...
                                                    @break
                                                @else
                                                    {{$subcategory->name}}
                                                @endif  
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li>
                        <a href="{{ url('notifications') }}">
                            <i class="fa fa-bell"></i>
                        </a>
                    </li>
                    <li><a href="{{ route('inbox')}}"><i class="fa fa-envelope"></i></a></li>
                    <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           Employee<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('sales/manage') }}">Manage sales</a></li>
                            <li><a href="{{ url('gigs/manage')}}">Manage gigs</a></li>
                            <li><a href="#">Earnings</a></li>
                            <li><a href="{{ url('requests/all') }}">Employer requests</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('create-gig') }}">
                                    <b>Create new gig <i class="fa fa-plus"></i></b>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Employer<span class="caret"></span></a>
                         <ul class="dropdown-menu" role="menu">
                             <li><a href="{{ url('orders/manage') }}">Manage orders</a></li>
                             <li><a href="{{ url('requests/manage') }}">Manage requests</a></li>
                             <li><a href="{{ url('team-account') }}">Group account</a></li>
                             <li class="divider"></li>
                             <li>
                                <a href="{{ url('requests/new') }}">
                                    <b>Post new request <i class="fa fa-plus"></i></b>
                                </a>
                            </li>
                         </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Balance: &#8358;{{number_format(Auth::user()->account_balance)}}</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('dashboard')}}">Dashboard</a></li>
                            <li><a href="{{ url('profile/'.Auth::user()->username) }}">Profile</a></li>
                            <li><a href="{{ url('settings') }}">Settings</a></li>
                            <li><a href="{{url("payments/addfunds")}}">Add funds</a></li>
                            <li><a href="{{url("payments/withdraw")}}">Withdraw</a></li>
                            <li class="divider"></li>
                             <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<nav id="helper-nav">
    <ul>
        @foreach($categories as $category)
            
            <li>
                @if($category->name == "Graphics & Design")
                    <i class="fa fa-paint-brush"></i>
                @endif
                @if($category->name == "Marketing")
                    <i class="fa fa-group"></i>
                @endif
                @if($category->name == "Writing & Translation")
                    <i class="fa fa-pencil"></i>
                @endif
                @if($category->name == "Video & Animation")
                    <i class="fa fa-camera-retro"></i>
                @endif
                @if($category->name == "Music & Audio")
                    <i class="fa fa-music"></i>
                @endif
                @if($category->name == "Programming & Tech")
                    <i class="fa fa-terminal"></i>
                @endif
                @if($category->name == "Business" || $category->name == "Consulting")
                    <i class="fa fa-briefcase"></i>
                @endif
                @if($category->name == "Fashion & Art")
                    <i class="fa fa-tags"></i>
                @endif
                 {{$category->name}}
            </li>
        @endforeach
    </ul>
</nav>
