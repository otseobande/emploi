<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Emploi') }} @yield('title')</title>

        <!-- Styles -->
      
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        @yield('style')
        <div id="app" v-cloak>
            
            @include('layouts.navbar')
            <div @click="hideCategories">
                @yield('content')
            </div>

        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            @auth
                app.accountBalance = {{Auth::user()->account_balance}}
            @endauth
        </script>
        @yield('scripts')
        @include('layouts.footer')
    </body>
</html>
