@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<a class="btn btn-primary pull-right" href="{{ url('requests/new')}}">Post a Request <i class="fa fa-plus"></i></a>
			<h3>Manage Requests</h3>

			<div class="col-md 12">
				<div class="card" style="overflow: auto">
					<vue-tabs>
						<v-tab title="Active">
							@if(count($active_requests) > 0)
								<table class="table table-striped table-responsive">
									<thead>
										<th>Date Posted</th>
										<th>Request Description</th>
										<th>Budget</th>
										<th>Delivery Period</th>
										<th>Offers</th>
										<th>Actions</th>
									
									</thead>
									<tbody>
										@foreach($active_requests as $active_request)
										<tr>
											<td>{{date("F jS, Y", strtotime($active_request->created_at))}}</td>
											<td>
												{{$active_request->description}}
											</td>
											<td>&#8358;{{number_format($active_request->budget)}}</td>
											<td>
												@if($active_request->delivery_period > 0)
													{{$active_request->delivery_period}} day(s)
												@else
													Unspecified
												@endif

											</td>
											<td>{{$active_request->offers()->count()}}</td>
											<td>
												<a class="btn btn-sm btn-primary" href="/requests/manage/{{$active_request->id}}" style="color: white;">View Offers</a>
												<button class="btn btn-sm btn-danger" @click="suspendRequest({{$active_request->id}})">Suspend</button>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							{{$active_requests->links()}}
							@else
								<div style="text-align: center">
									<p>No records found</p>
								</div>
							@endif
						</v-tab>
						<v-tab title="Accepted">
							@if(count($accepted_requests) > 0)
								<table class="table table-striped table-responsive">
									<thead>
										<th>Date Posted</th>
										<th>Request Description</th>
										<th>Budget</th>
										<th>Delivery Period</th>
										<th>Offers</th>
										<th>Actions</th>
									
									</thead>
									<tbody>
										@foreach($accepted_requests as $request)
										<tr>
											<td>{{date("F jS, Y", strtotime($request->created_at))}}</td>
											<td>
												{{$request->description}}
											</td>
											<td>&#8358;{{number_format($request->budget)}}</td>
											<td>
												@if($request->delivery_period > 0)
													{{$request->delivery_period}} day(s)
												@else
													Unspecified
												@endif

											</td>
											<td>{{$request->offers()->count()}}</td>
											<td>
												<a class="btn btn-sm btn-primary" href="/requests/manage/{{$request->id}}" style="color: white;">View Offers</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							{{$accepted_requests->links()}}
							@else
								<div style="text-align: center">
									<p>No records found</p>
								</div>
							@endif
						</v-tab>
						<v-tab title="Suspended">
							@if(count($suspended_requests) > 0)
								<table class="table table-striped">
									<thead>
										<th>Date Posted</th>
										<th>Request Description</th>
										<th>Budget</th>
										<th>Delivery Period</th>
										<th>Offers</th>
										<th></th>
										<th></th>
									</thead>
									<tbody>
										@foreach($suspended_requests as $suspended_request)
										<tr>
											<td>{{date("F jS, Y", strtotime($suspended_request->created_at))}}</td>
											<td>
												{{$suspended_request->description}}
											</td>
											<td>&#8358;{{number_format($suspended_request->budget)}}</td>
											<td>
												@if($suspended_request->delivery_period > 0)
													{{$suspended_request->delivery_period}} day(s)
												@else
													Unspecified
												@endif

											</td>
											<td>{{$suspended_request->offers()->count()}}</td>
											<td>
												<a class="btn btn-sm btn-primary" href="/request/manage/{{$suspended_request->id}}" style="color: white;">View Offers</a>
											</td>
											<td>
												<button class="btn btn-sm btn-danger" @click="activateRequest({{$suspended_request->id}})">Activate</button>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							{{$suspended_requests->links()}}
							@else
								<div style="text-align: center">
									<p>No records found</p>
								</div>
							@endif
						</v-tab>
					</vue-tabs>
				</div>
			</div>
		</div>
	</div>
@endsection