@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Offers</h3>
				<el-card style="overflow: auto;">
					<div class="row" style="margin-top: 10px;">
						<div class="col-md-4">
							<p><b>Request description: </b></p>
						</div>
						<div class="col-md-8">
							<p><i>"{{$request->description}}"</i></p>
						</div>
					</div>
					<div style="margin-top: 10px;">
						<table class="table table-striped table-hover table-responsive">
							<thead>
								<tr>
									<th>User</th>
									<th>Details</th>
									<th>Price</th>
									<th>Delivery Period</th>
									<th>Date</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($request->offers as $offer)
								<tr>
									<td>
										<a href={{url('profile/'.$offer->user->username)}}>{{$offer->user->username}}</a>
									</td>
									<td>{{$offer->details}}</td>
									<td>&#8358;{{number_format($offer->price)}}</td>
									<td>{{$offer->delivery_period}}</td>
									<td>{{date("F jS, Y", strtotime($offer->created_at))}}</td>
									<td>
										@if($offer->accepted == 0)
											@if($request->accepted == 0)
												<button class="btn btn-sm btn-success" @click="acceptOffer({{$offer->id}})">Accept</button>
											@else
												<button class="btn btn-sm btn-default">Accept</button>
											@endif
										@else
											<button class="btn btn-sm btn-success" disabled>Accepted</button>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</el-card>
			</div>
		</div>
	</div>
@endsection