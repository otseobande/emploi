@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3><strong>Manage Orders</strong></h3>
			<div class="card">
				<vue-tabs>
					<v-tab title="Active">
						@if($active_orders->count() > 0)
							<table class="table table-striped table-hover">
								<thead>
									<th>Id</th>
									<th>Gig title</th>
									<th>Price</th>
									<th>Delivery date</th>
									<th>Actions</th>
								</thead>
								<tbody>
									
										@foreach($active_orders as $order)
										<tr>
											<td>{{$order->id}}</td>
											<td>{{$order->gig->title}}</td>
											<td>&#8358;{{number_format($order->price)}}</td>
											<td>{{date("M jS, Y", strtotime($order->delivery_date))}}</td>
											<td>
												<a style="color: white" class="btn btn-primary btn-xs" href="{{url('conversations/'.$order->gig->user->username)}}">contact user</a>
											</td>
										<tr>
										@endforeach
								</tbody>
							</table>
						@else
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
					<v-tab title="Late">
						<div style="text-align: center">
								<p>No records found</p>
							</div>
					</v-tab>
					<v-tab title="Delivered">
						@if($delivered_orders->count() > 0)
							<table class="table table-striped table-hover">
								<thead>
									<th>Id</th>
									<th>Gig title</th>
									<th>Price</th>
									<th>Delivery date</th>
									<th>Actions</th>
								</thead>
								<tbody>
									
										@foreach($delivered_orders as $order)
										<tr>
											<td>{{$order->id}}</td>
											<td>{{$order->gig->title}}</td>
											<td>&#8358;{{number_format($order->price)}}</td>
											<td>{{date("M jS, Y", strtotime($order->delivery_date))}}</td>
											<td></td>
										<tr>
										@endforeach
									
								</tbody>
							</table>
						@else
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
					<v-tab title="Completed">
						@if($completed_orders->count() > 0)
							<table class="table table-striped table-hover">
								<thead>
									<th>Id</th>
									<th>Gig title</th>
									<th>Price</th>
									<th>Delivery date</th>
									<th>Actions</th>
								</thead>
								<tbody>
										@foreach($completed_orders as $order)
										<tr>
											<td>{{$order->id}}</td>
											<td>{{$order->gig->title}}</td>
											<td>{{$order->price}}</td>
											<td>{{$order->delivery_date}}</td>
											<td></td>
										<tr>
										@endforeach
								</tbody>
							</table>
						@else
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
					<v-tab title="Cancelled">
						@if($cancelled_orders->count() > 0)
							<table class="table table-striped table-hover">
								<thead>
									<th>Id</th>
									<th>Gig title</th>
									<th>Price</th>
									<th>Delivery date</th>
									<th>Actions</th>
								</thead>
								<tbody>
									
										@foreach($cancelled_orders as $order)
										<tr>
											<td>{{$order->id}}</td>
											<td>{{$order->gig->title}}</td>
											<td>{{$order->price}}</td>
											<td>{{$order->delivery_date}}</td>
											<td></td>
										<tr>
										@endforeach
					
								</tbody>
							</table>
						@else
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
				</vue-tabs>
			</div>
		</div>
	</div>
@endsection