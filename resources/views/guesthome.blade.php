@extends('layouts.app')

@section('style')
<style>
    #main-nav{
        margin-bottom: 0px !important;
        border-width: 0px;
    }
    
</style>
@endsection

@section('content')
<div class="home-images">
    <div style="margin: auto; padding-top: 150px; color: white;">
        <center>
            <h1 style="margin-top: 0px">Welcome to Emploi</h1>
            <p style="font-size: 20px">The service network store</p>
            <form class="form-inline" action="{{route('search')}}">
                <div class="input-group">
                    <input type="text" name="q" id="guestsearch" placeholder="Search jobs or people" class="form-control jobsearch">
                    <span class="input-group-btn">
                       <button class="btn btn-default" style="height: 35px;background-color: beige;" type="submit">
                          <i class="fa fa-search"></i>
                       </button>
                    </span>
                </div>   
            </form>
        </center>
    </div>
</div>
<div class="plain-row">
    <div class="container">
        <div class="row">
            <center>
                <h2><b>Explore our categories</b></h2>
                <p>Build your business properly</p>
            </center>
            <div style="text-align: center;">
                <div class="col-md-3">
                    <div class="card" style="padding: 10px;">
                        <img src="img/programming-icon.png">
                        <p>Programming and Tech</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card" style="padding: 10px;">
                        <img src="img/graphics-icon.png">
                        <p>Graphics and Design</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card" style="padding: 10px;">
                        <img src="img/marketing-icon.png">
                        <p>Digital Marketing</p>
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="card" style="padding: 10px;">
                        <img src="img/writing-icon.png">
                        <p>Writing and Translation</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="" class="info-row">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1>Your <b>Terms</b></h1>
                <p>Whatever you need to simplify your to do list, no matter your budget.</p>
            </div>
            <div class="col-md-4">
                <h1>Your <b>Timeline</b></h1>
                <p>Find services based on your goals and deadlines, it’s that simple.</p>
            </div>
            <div class="col-md-4">
                <h1>Your <b>Safety</b></h1>
                <p>Your payment is always secure, Emploi is built to protect your peace of mind.</p>
            </div>
        </div>
        <div class="row">
            <center>
                <a href="{{url('register')}}" class="btn btn-lg btn-primary">Register Now</a>
            </center>
        </div>
    </div>
</div>

<footer id="main-footer" role="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul>
                    <li>CATEGORIES</li>
                    <br>
                    @foreach($categories as $category)
                        <li>{{$category->name}}</li>
                    @endforeach
                    
                </ul>
            </div>
            <div class="col-md-3">
                <h5>ABOUT</h5>
            </div>
            <div class="col-md-3">
                <h5>SUPPORT<h5>
            </div>
            <div class="col-md-3" id="follow-us">
                <ul>
                    <li>
                        FOLLOW US
                    </li>
                    <br>
                    <li>
                        <a href="http://facebook.com"><span class="fa fa-facebook"></span>  Facebook</a>
                    </li>
                    <li>
                        <a href="http://twitter.com"><span class="fa fa-twitter"></span> Twitter</a>
                    </li>
                    <li>
                        <a href="http://facebook.com"><span class="fa fa-instagram"></span>  Instagram</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
@endsection
