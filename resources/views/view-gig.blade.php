@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<el-card style="margin-bottom: 10px;">
					@if($gig->user->id != Auth::user()->id)
						<h2>{{$gig->title}} <button class="btn btn-success" @click="gigOrderDialogVisible = true">Order Now</button></h2>
						
							<div>
								<img class="img-rounded" width="300" src="{{ asset('storage/'.$gig->image)}}">
							</div>
							<div>
								<p><b>Price: </b>&#8358;{{$gig->price}}</p>
								<p><b>About Gig: </b>{{$gig->description}}</p>
							</div>
						
					@else
						<h2>You cannot order your own gig</h2>
					@endif
				</el-card>
				<div>
					@if($gig->user->gigs->count() > 1)
						<h3>Other gigs from user</h3>
						@foreach($gig->user->gigs as $other_gig)
							@if($gig->id != $other_gig->id)
								<div class="col-sm-5 col-md-3">	
									<div class="card gig-block">
										<div class="gig-block-image">
											<img src="{{ asset('storage/'.$other_gig->image)}}">
										</div>
										<div class="gig-block-body">
											<div class="row">
												<div class="col-xs-3" style="padding-right: 0px;">
													<img class="img-circle img-responsive user-img" src="{{ asset('img/profile/user.png')}}">
												</div>
												<div class="col-xs-9">
													 <span class="user"><a href="{{url('profile/'.$other_gig->user->username)}}">{{$other_gig->user->username}}</a></span><br>
													{{--  <el-rate 
													 	style="display: inline;"
														:value="{{$other_gig->user->ratings()}}"
														style="margin: auto; width: 121px;"
													    :colors="['#99A9BF', '#F7BA2A', '#FF9900']" 
													    disabled>
													 </el-rate> --}}
												</div>
											</div>
											<div class="title">
												<a href="{{url('orders/gig/'.$other_gig->id)}}">{{$other_gig->title}}</a>
											</div>
										</div>
										<div class="gig-block-footer">
											<span class="" style="font-size: 12px">From: &#8358;{{$other_gig->price}}</span>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<section class="content-box">
					<div style="margin: auto;">
						
						<img class="img-circle img-responsive profile_img" src="{{ asset('storage/'.$gig->user->profile_image_link) }}">
						<br>	
						<el-rate 
							:value="4"
							style="margin: auto; width: 121px;"
						    :colors="['#99A9BF', '#F7BA2A', '#FF9900']" 
						    disabled>
						 </el-rate><br>
						<div>
							<a style="margin-left: 57px;" class="btn btn-primary" href="{{url("conversations/".$gig->user->username)}}">Contact employee</a>
						</div>
					</div>
					<br>
					<table class="user-stats">
						<tr class="location">
							<td><span class="fa fa-map-marker pull-left"></span></td>
							<td>From</td>          
							<td><strong class="pull-right">Nigeria</strong></td>
						</tr>
						<tr class="member-since">
							<td><span class="fa fa-user pull-left"></span></td>
							<td>Member Since</td>
							<td><strong class="pull-right">{{date("F, Y", strtotime($gig->user->created_at))}}</strong></td>
						</tr>
						{{-- <tr class="average-response-time">
							<td><span class="fa fa-clock-o pull-left"></span></td>
							<td>Average Response Time</td>
							<td></td>
						</tr> --}}
					</table>
				</section>
				<section class="content-box">
					<h5>
						<strong>Description</strong>
						
					</h5>
					<div class="row">
						<div class="col-md-12">
							<p v-if="!edittingDescription">{{$gig->user->description}}</p>
							<div v-else>
								<form method="POST" action="{{url('profile/edit/description')}}">
									{{csrf_field()}}
									<textarea class="form-control" placeholder="{{$gig->user->description}}" name="description"></textarea>
									<br>
									<div class="row">
										<div class="col-md-6">
											<button class="btn btn-default btn-block" @click.prevent="edittingDescription = false">Cancel</button>
										</div>
										<div class="col-md-6">
											<button type="submit" class="btn btn-success btn-block">Edit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
				</section>
			</div>
		</div>
	</div>

	<el-dialog
	  title="Gig Order"
	  :visible.sync="gigOrderDialogVisible"
	  width="50%"
	  >
	  @if(Auth::user()->account_balance < $gig->price)
	  	<span>You do not have sufficient balance to order this gig. Please click the add funds button below to fund your account.</span>
	  @else
	  	<span><b>&#8358;{{$gig->price}}</b> would be deducted from your account but would not be sent to the employer until the job is confirmed done.</span>
	  @endif
	  <span slot="footer" class="dialog-footer">
	    <el-button @click="gigOrderDialogVisible = false">Cancel</el-button>
	    @if(Auth::user()->account_balance < $gig->price)
	    	<a class="btn btn-primary" href="{{url('payments/addfunds')}}">Add Funds</a>
	    @else
	    	<el-button type="primary" @click="orderGig({{$gig->id}})">Proceed</el-button>
	    @endif
	  </span>
	</el-dialog>
@endsection