@extends('layouts.app')

@section('title')
 | {{Auth::user()->username}}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<header>
				<h1>Conversations</h1>
			</header>
		</div>
		<div class="row">
			
			<div class="col-md-12 card">

				<table class="inbox-table table table-hover table-striped">
					<thead>
						<tr>
							<td><input type="checkbox" name=""><span class="caret"></span></td>
							<td>User</td>
							<td>Last Message</td>
							<td>Date sent</td>
						</tr>
					</thead>
					<tbody>
						@if($threads->count() > 0)
							
							@foreach($threads as $thread)
								<tr @click="openConversation('{{$thread->participants()->where('user_id', '!=',Auth::user()->id)->get()->first()->user->username}}','{{$thread->id}}')">
									<td><input type="checkbox" name=""><span class="caret"></span></td>
									<td>
										{{$thread->participants()->where('user_id', '!=',Auth::user()->id)->get()->first()->user->username}}
									</td>
									<td>
										{{$thread->getLatestMessageAttribute()->body}}
									</td>
									<td>
										{{date("D, d M y H:i:s", strtotime($thread->getLatestMessageAttribute()->updated_at))}}
									</td>
								</tr>
							@endforeach
						@else
							No records found
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection