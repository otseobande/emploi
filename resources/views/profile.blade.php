@extends('layouts.app')

@section('title')
	| {{ $user->username }}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<section class="content-box">
					<div style="margin: auto;">
						<label class="user-pic" >
							<img class="img-circle img-responsive profile_img" src="{{ asset('storage/'.$user->profile_image_link) }}" @click.prevent="displayUploadDialog">
							<form ref="profilePicUploadForm">
								<input type="file" @change.prevent="changeProfilePic" accept="image/png,image/jpeg" class="hidden" id="profile-pic-upload" name="profileImg">
							</form>
						</label>
							
						
						<el-rate 
							:value="4"
							style="margin: auto; width: 121px;"
						    :colors="['#99A9BF', '#F7BA2A', '#FF9900']" 
						    disabled>
						 </el-rate>
						
					</div>
					@if($own_profile)
						{{-- <form action="/profile/uploadavatar" method="POST" enctype="multipart/form-data">
							<input type="file" name="avatar" id="avatar">
							<button type="submit">Upload</button>
						</form> --}}
					@endif
					@if(!$own_profile)
						<a class="btn btn-success btn-block" href="{{url('conversations/'.$user->username)}}">Contact</a>
					@endif<br>
					<table class="user-stats">
						<tr class="location">
							<td><span class="fa fa-map-marker pull-left"></span></td>
							<td>From</td>          
							<td><strong class="pull-right">Nigeria</strong></td>
						</tr>
						<tr class="member-since">
							<td><span class="fa fa-user pull-left"></span></td>
							<td>Member Since</td>
							<td><strong class="pull-right">{{date("F, Y", strtotime($user->created_at))}}</strong></td>
						</tr>
						{{-- <tr class="average-response-time">
							<td><span class="fa fa-clock-o pull-left"></span></td>
							<td>Average Response Time</td>
							<td></td>
						</tr> --}}
					</table>
				</section>
				<section class="content-box">
					<h5>
						<strong>Description</strong>
						@if($own_profile)
							<a class="pull-right" @click="edittingDescription = !edittingDescription">edit</a>
						@endif
					</h5>
					<div class="row">
						<div class="col-md-12">
							<p v-if="!edittingDescription">{{$user->description}}</p>
							<div v-else>
								<form method="POST" action="{{url('profile/edit/description')}}">
									{{csrf_field()}}
									<textarea class="form-control" name="description">{{$user->description}}</textarea>
									<br>
									<div class="row">
										<div class="col-md-6">
											<button class="btn btn-default btn-block" @click.prevent="edittingDescription = false">Cancel</button>
										</div>
										<div class="col-md-6">
											<button type="submit" class="btn btn-success btn-block">Edit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<hr>
					<h5>
						<strong>Skills</strong>
						@if($own_profile)
							<a class="pull-right" @click="edittingSkills = !edittingSkills">add</a>
						@endif
					</h5>
					<div class="row">
						<div v-if="!edittingSkills" class="col-md-12">
							<el-tag
							  :key="skill.id"
							  v-for="skill in skills"
							  closable
							  :disable-transitions="false"
							  @close="removeSkill(skill.id)">
							  @{{skill.name}}
							</el-tag>
							
						</div>
						<div v-else class="col-md-12">
							<form method="POST" action="{{url('profile/add/skill')}}" @keyup.enter.prevent="this.submit()">
								{{csrf_field()}}
								<input  class="form-control" placeholder="Add skill" name="skill-name"><br>
								<select class="form-control" name="skill-experience-level">
									<option value="Beginner">Beginner</option>
									<option value="Intermediate">Intermediate</option>
									<option value="Expert">Expert</option>
								</select>
								<br>
								<div class="row">
									<div class="col-md-6">
										<button class="btn btn-default btn-block" @click.prevent="edittingSkills = false">Cancel</button>
									</div>
									<div class="col-md-6">
										<button type="submit" class="btn btn-success btn-block">Add</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<hr>
					<h5>
						<strong>Education</strong>
						@if($own_profile)
							<a class="pull-right">add</a>
						@endif
					</h5>
					<div class="row">
						<div>
							@foreach($user->education()->get() as $education)
							@endforeach
						</div>
					</div>
					<hr>
					<h5>
						<strong>Certification</strong>
						@if($own_profile)
							<a class="pull-right">add</a>
						@endif
					</h5>
					<div class="row">
						<div>
							@foreach($user->certifications()->get() as $certification)
								{{$certification}}
							@endforeach
						</div>
					</div>
				</section>
			</div>

			<div class="col-md-8">
				<section class="flat-content-box">
					<p style="font-size: 20px;">
						<strong>{{$user->username}}'s Gigs</strong>
						@if($own_profile)
							<a class="btn btn-lg btn-primary pull-right" href="{{url('gigs/create')}}">
								Create Gig <i class="fa fa-plus"></i>
							</a>
						@endif
					</p>
					
				</section><br>

				<div class="row">
					@foreach($user->gigs as $gig)
					<div class="col-sm-5 col-md-3">	
						<div class="card gig-block">
							<div class="gig-block-image">
								<img src="{{ asset('storage/'.$gig->image)}}">
							</div>
							<div class="gig-block-body">
								<div class="row">
									<div class="col-xs-3" style="padding-right: 0px;">
										<img class="img-circle img-responsive user-img" src="{{ asset('img/profile/user.png')}}">
									</div>
									<div class="col-xs-9">
										 <span class="user"><a href="{{url('profile/'.$gig->user->username)}}">{{$gig->user->username}}</a></span><br>
										{{--  <el-rate 
										 	style="display: inline;"
											:value="{{$gig->user->ratings()}}"
											style="margin: auto; width: 121px;"
										    :colors="['#99A9BF', '#F7BA2A', '#FF9900']" 
										    disabled>
										 </el-rate> --}}
									</div>
								</div>
								<div class="title">
									<a href="{{url('orders/gig/'.$gig->id)}}">{{$gig->title}}</a>
								</div>
							</div>
							<div class="gig-block-footer">
								<span class="" style="font-size: 12px">From: &#8358;{{$gig->price}}</span>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		app.skills = {!!json_encode($user->skills)!!};
	</script>
@endsection