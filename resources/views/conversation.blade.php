@extends('layouts.app')

@section('content')

		<div class="container">
		<div class="row">
			<header>
				<h1>Inbox</h1>
			</header>
		</div>
		<div class="row">
			@if($employment_orders || $employer_orders)
				<el-card>
					<h3><b>Current gigs with {{$user->username}}</b></h3>
					@foreach($employment_orders as $order)
					<div class="row">
						<el-card>
							<div class="col-md-2">
								<h5>
									<b>Gig title</b>
								</h5>
								<p>{{$order->gig->title}}</p>
							</div>
							<div class="col-md-2">
								<h5>
									<b>Price</b>
								</h5>
								<p>&#8358;{{number_format($order->price)}}</p>
							</div>
							<div class="col-md-2">
								<h5>
									<b>Time Left</b>
								</h5>
								@if($order->delivered)
									Delivered
								@else
									<countdown to="{{$order->delivery_date}}"></countdown>
								@endif
								
							</div>
							<div class="col-md-2">
								<h5>
									<b>Status</b>
								</h5>
								
								@if($order->delivered)
									<p style="color: green;"><b>Delivered</b> <br>(would be marked as completed 3 days from delivery)</p>
								@else
									<p>Pending delivery</p>
								@endif
								
							</div>
							<div class="col-md-2">
								<h5>
									<b>Actions</b>
								</h5>
								@if($order->delivered)
									<button disabled class="btn btn-sm btn-success">Delivered</button>
								@else
									<button class="btn btn-sm btn-primary" @click="showDeliveryDialog({{$order->id}})">Deliver <i class="fa fa-upload"></i></button>
								@endif
								<button class="btn btn-sm btn-danger" @click="employeeCancelOrderDialogVisible = true">Cancel</button>
							</div>
						</el-card>
					</div>
					@endforeach

					@if($employer_orders)
						<h4>current gigs as an employer:</h4>
					@endif
					@foreach($employer_orders as $order)
					<div class="row">
						<el-card>
							<div class="col-md-2">
								<h5>
									<b>Gig title</b>
								</h5>
								<p>{{$order->gig->title}}</p>
							</div>
							<div class="col-md-2">
								<h5>
									<b>Price</b>
								</h5>
								<p>&#8358;{{number_format($order->price)}}</p>
							</div>
							<div class="col-md-2">
								<h5>
									<b>Time Left</b>
								</h5>
								@if($order->delivered)
									Delivered
								@else
									<countdown to="{{$order->delivery_date}}"></countdown>
								@endif
							</div>
							<div class="col-md-2">
								<h5>
									<b>Status</b>
								</h5>
								@if($order->delivered)
									<p style="color: green;"><b>Delivered</b> <br>(would be marked as completed 3 days from delivery)</p>
								@else
									<p>Pending delivery</p>
								@endif
							</div>
							<div class="col-md-2">
								<h5>
									<b>Actions</b>
								</h5>
								@if($order->delivered)
									<a class="btn btn-sm btn-primary" download="emploi_delivery" 
									href="{{url('storage/'.$order->deliveries->last()->attachment_link)}}">Download delivery <i class="fa fa-download"></i></a>
									<button class="btn btn-sm btn-warning" 
									@click="showRequestModificationDialog({{$order->id}})">Request modification</button>
									<button class="btn btn-sm btn-success">Mark as completed</button>
								@endif
								<button class="btn btn-sm btn-danger" 
								@click="showCancelOrderDialog({{$order->id}})">Cancel</button>
							</div>

						</el-card>
					</div>
					@endforeach

				</el-card><br>
			@endif
			<div class="col-md-11 col-md-offset-1" style="padding-bottom: 30px;">
				<el-card >
					<h4>Conversation with <b>{{$user->username}}</b></h4>
					<div class="messages" style="max-height: 500px; overflow: auto;">

						@foreach($messages as $message)

							@if($message->user_id == Auth::user()->id)
								<div class="sent-message">
									<div class="row">
										<div class="col-md-1">
											<img class="img-circle" width="40" src="{{asset('storage/'.$message->user->profile_image_link)}}">
										</div>
										<div class="col-md-11">
											<div style="color: black;">
												<p><b>Me</b></p>
												<p>{{$message->body}}</p>
												@if($message->attachments->count() > 0)
													<span class="pull-right">
														<h5>{{$message->attachments->first()->filename}}</h5>
														<i class="fa fa-file fa-2x"></i><br>
														<a download="emploi" class="btn btn-primary btn-xs" 
														href="{{url('storage/'.$message->attachments->first()->link)}}">download</a>
													</span>
												@endif
											</div>
											<p style="font-size: 10px;">{{date("D, d M y H:i:s", strtotime($message->created_at))}}</p>


										</div>
									</div>
								</div>
							@else
								<div class="recieved-message">
									<div class="row">
										<div class="col-md-1">
											<img class="img-circle" width="40" src="{{asset('storage/'.$user->profile_image_link)}}">
										</div>
										<div class="col-md-11">
											<div style="color: black;">
												<p>
													<a href="{{url('profile/'.$user->username)}}"><b>{{$user->username}}</b></a>
												</p>
												<p>{{$message->body}}</p>
												@if($message->attachments->count() > 0)
													<span class="pull-right">
														<h5>{{$message->attachments->first()->filename}}</h5>
														<i class="fa fa-file fa-2x"></i><br>
														<a download="emploi" class="btn btn-primary btn-xs" 
														href="{{url('storage/'.$message->attachments->first()->link)}}">download</a>
													</span>
												@endif
												<p style="font-size: 10px;">{{date("D, d M y H:i:s", strtotime($message->created_at))}}</p>
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
						
					</div>
					<div class="">
						
						<form method="POST" ref="messageForm" action="{{url('conversations/'.$user->username.'/send')}}">
							{{csrf_field()}}
							@if($thread)
								<input type="hidden" name="thread-id" value="{{$thread->id}}">
							@endif
							<textarea class="form-control" name="message" style="margin-top: 10px; border-width: 1px; border-style: solid; border-radius: 10px;" placeholder="Enter message here" v-model="message.text"></textarea>
							<button class="btn btn-success" type="button" @click.prevent="sendMessage('{{$user->username}}')" style="margin-top: 5px;">Send</button>
							<button @click="uploadMessageAttachment" style="margin-top: 5px;" type="button" class="btn btn-primary">Attach files <i class="fa fa-upload"></i></button> 
							<input type="file" @change="bindUploadedFiles" name="attachment" id="attachment-upload" class="hidden">
							<span v-for="file in message.files"><i class="fa fa-file"></i> @{{file.name}}</span>
							<button  style="margin-top: 5px;" type="button" class="btn btn-primary pull-right" @click="customOfferDialogVisible = true">Custom offer <i class="fa fa-book"></i></button>
						</form>
						@if(Auth::user()->myCustomOffers || Auth::user()->employeeCustomOffers)
						<div class="custom-offers">
							@if(Auth::user()->myCustomOffers->count() > 0)
								<h5><b>My custom Offers</b></h5>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Details</th>
											<th>Price</th>
											<th>status</th>
											<th>action</th>
										</tr>
									</thead>
									<tbody>
										@foreach(Auth::user()->myCustomOffers as $customOffer)
											<tr>
												<td>{{$customOffer->details}}</td>
												<td>&#8358;{{number_format($customOffer->price)}}</td>
												<td>
													@if(!$customOffer->accepted)
														Pending
													@endif
												</td>
												<td>
													<button class="btn btn-sm btn-primary" @click="withdrawCustomOffer({{$customOffer->id}})">Withdraw</button>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							@endif
							@if(Auth::user()->employeeCustomOffers->count() > 0)
								<h5><b>Employee custom Offers</b></h5>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Details</th>
											<th>Price</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@foreach(Auth::user()->employeeCustomOffers as $customOffer)
											<tr>
												<td>{{$customOffer->details}}</td>
												<td>&#8358;{{number_format($customOffer->price)}}</td>
												<td>
													<button class="btn btn-sm btn-success" @click="acceptCustomOffer({{$customOffer->id}})">Accept</button>
													<button class="btn btn-sm btn-danger">Reject</button>
												</td> 
											</tr>
										@endforeach
									</tbody>
								</table>
							@endif
						</div>
						@endif
					</div>
				</el-card>
			</div>
		</div>
	</div>zx
	<el-dialog
	  title="Custom Offer"
	  :visible.sync="customOfferDialogVisible"
	  width="50%"
	  >
	  	<form ref="customOffer" action="{{url('/customoffer/create')}}" method="POST">
	  		{{csrf_field()}}
	  		<input type="hidden" name="employer-id" value="{{$user->id}}">
		  	<div>
		  		<label>Gig*:</label>
			  	<select class="form-control" name="gig-id">
			  		<option disabled selected>Select Gig</option>
			  		@foreach(Auth::user()->gigs as $gig)
			  			<option value="{{$gig->id}}">{{$gig->title}}</option>
			  		@endforeach
			  	</select>
		  	</div>
		  	<div>
			  <label>Details:</label>
			  <textarea name="details" class="form-control"></textarea>
			</div>
			<div>
				<label>Delivery Period:</label>
				<select name="delivery-period" class="form-control">
					<option disabled selected>Select Period</option>
					@for($i = 1; $i <= 20; $i++)
						<option value="{{$i}}">{{$i}} day
							@if($i > 1 )
							s
							@endif
						</option>
					@endfor
				</select>
			</div>
			<div>
				<label >Price:</label>
				<input type="number" name="price" class="form-control" min="1000">
			</div>
	    </form>
	  	<span slot="footer" class="dialog-footer">
		  	<button @click="$refs.customOffer.submit()" class="btn btn-primary">Submit Offer</button>
		    <button type="button" class="btn btn-default" @click="customOfferDialogVisible = false">Cancel</button>
	  	</span>
		
	</el-dialog>
	<el-dialog
	  title="Deliver order"
	  :visible.sync="deliverDialogVisible"
	  width="50%"
	  >
	  	<form ref="deliveryForm">
		  	<div>
		  		<label>Upload work*:</label>
			  	<input type="file" id="delivery" name="delivery" class="form-control" @change="uploadedFile">
		  	</div>
		  	<div>
			  <label>Description:</label>
			  <textarea name="description" class="form-control"></textarea>
			</div>
	    </form>
	  	<span slot="footer" class="dialog-footer">
		  	<button type="submit" class="btn btn-primary" :disabled="!deliveredFile" @click.prevent="deliverOrder">Submit</button>
		    <button type="button" class="btn btn-default" @click="deliverDialogVisible = false">Cancel</button>
	  	</span>
		
	</el-dialog>
	<el-dialog
	  title="Cancel order"
	  :visible.sync="employeeCancelOrderDialogVisible"
	  width="50%"
	  >
	  	<h3>Are you sure you want to cancel order?</h3>
	  	<div>
	  		<label>Reason for cancellation: </label>
	  		<textarea name="reason" class="form-control"></textarea>
	  	</div>
	  	<span slot="footer" class="dialog-footer">
		  	<button type="submit" class="btn btn-danger" @click.prevent="cancelOrder">Yes</button>
		    <button type="button" class="btn btn-default" @click="employeeCancelOrderDialogVisible = false">No</button>
	  	</span>
		
	</el-dialog>
	<el-dialog
	  title="Deliver order"
	  :visible.sync="employerCancelOrderDialogVisible"
	  width="50%"
	  >
	  	<h3>Are you sure you want to cancel order?</h3>

	  	<div>
	  		<label>Reason: </label>
	  		<textarea name="reason" class="form-control"></textarea>
	  	
	  	<p>
		  	<b>Note: </b> Only 75% of order amount would be refunded if the employee does not accept the cancellation within 72 hours (3 days).
		</p>
		</div>
	  	<span slot="footer" class="dialog-footer">
		  	<button type="submit" class="btn btn-danger" @click.prevent="cancelOrder">Yes</button>
		    <button type="button" class="btn btn-default" @click="employerCancelOrderDialogVisible = false">No</button>
	  	</span>
		
	</el-dialog> 
	<el-dialog
	  title="Request Modification"
	  :visible.sync="modificationRequestDialogVisible"
	  width="50%"
	  >
	  	<div>
	  		<label>Reason for modification: </label>
	  		<textarea name="modification" v-model="modificationReason" class="form-control"></textarea>
	  	</div>
	  	<span slot="footer" class="dialog-footer">
		  	<button type="submit" class="btn btn-primary" @click.prevent="requestModification">Submit</button>
		    <button type="button" class="btn btn-default" @click="modificationRequestDialogVisible = false">Cancel</button>
	  	</span>
		
	</el-dialog>
@endsection

@section('scripts')
<script>
	let msg = document.querySelector('.messages');
	msg.scrollTo(0,msg.scrollHeight);
</script>
@endsection