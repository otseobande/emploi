@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<el-card>
					<div class="row">
						<div class="col-md-2">
							<img class="img-rounded" href="{{url($request->user->profile_image_link)}}">
						</div>
						<div class="col-md-10">
							<h4><b>Request description:</b></h4>
							<p>"<i>{{$request->description}}</i>"</p>
							@if($request->attachment)
										<a href="{{ url('storage/attachments'.$request->attachment->link)}}">Download Attachment</a>
									@endif
						</div>
					</div>

					@if(count($request->user->gigs()->get()) > 0)
						<form method="POST" action="{{url('requests/offer/make')}}">
							{{csrf_field()}}
							<input type="hidden" name="request-id" value="{{$request->id}}">
							<div class="form-group">
								<label for="gig" class="col-sm-2 control-label">Gig to Offer: </label>
								<div class="col-sm-10">
									<select class="form-control gig-title" id="gig-title" name="gig-id" required>
										<option disabled selected>Select one</option>
										@foreach($request->user->gigs()->get() as $gig)
											<option value="{{$gig->id}}">{{$gig->description}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="gig-details" class="col-sm-2 control-label">Details: </label>
								<div class="col-sm-10">
									<textarea class="form-control gig-title" id="gig-details" name="details" required>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="gig" class="col-sm-2 control-label">Offer Amount: </label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">&#8358;</span>
										<input type="number" name="amount" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="gig" class="col-sm-2 control-label">Delivery Period: </label>
								<div class="col-sm-10">
									<select class="form-control" name="delivery-period">
										<option disabled selected>Select one</option>
										@for($i = 0; $i<=20; $i++)
											<option value="{{$i}}">{{$i}} days</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="gig" class="col-sm-2 control-label">Number of revisions (optional): </label>
								<div class="col-sm-10">
									<select class="form-control" name="delivery_period">
										<option disabled selected>Select one</option>
										@for($i = 0; $i<=20; $i++)
											<option value="{{$i}}">{{$i}}</option>
										@endfor
									</select>
								</div>
							</div>
							<button class="btn btn-primary pull-right">Make Offer</button>
						</form>
					@else
						<div style=" padding: 10px; border-style: solid; border-width: 1px; background-color: gray; color: white; border-radius: 5px; margin: auto;">
							<p>Please create a gig to make offers</p>
							<a href="{{url('gigs/create')}}" class="btn btn-primary">Create Gig</a>
						</div>
					@endif
			</el-card>
			</div>
		</div>
	</div>
@endsection