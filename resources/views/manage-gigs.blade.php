@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2><strong>Gigs</strong></h2>
				<a class="btn btn-primary pull-right" href="{{ route('create-gig')}}">Create New Gig <i class="fa fa-plus"></i></a>
			</div>
			
		</div><br>
		<div class="row">
			<div class="col-md-12 card" style="text-align: left;">
				<vue-tabs>
					<v-tab title="Active">
						<table class="table table-striped">
							<thead>
								<th></th>
								<th>Title</th>
								<th>Created</th>
								<th>Actions</th>
							</thead>
							<tbody>
							@foreach($active_gigs as $gig)
							<tr>
								<td>
									<img src="{{url('storage/'.$gig->image)}}" width="50">
								</td>
								
								<td>
									<p>{{$gig->title}}</p><br>
									{{-- <img style="padding-right: 20px;" class="img-circle" width="50" height="50" src="{{asset('storage/'.$active_gig->image)}}">		 --}}
								</td>
								<td>{{date("M jS, Y", strtotime($gig->created_at))}}</td>
								<td>
									<button class="btn btn-primary btn-sm" @click="redirect('/gigs/edit/{{$gig->id}}')" type="button">Edit</button>
									<button class="btn btn-warning btn-sm" type="button" @click="pauseGig({{$gig->id}})">Pause</button>
									<button class="btn btn-danger btn-sm" type="button" @click="cancelGig({{$gig->id}})">Delete</button>
								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						@if($active_gigs->count() < 1)
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
					<v-tab title="Paused">
						<table class="table table-striped">
							<thead>
								<th></th>
								<th>Title</th>
								<th>Created</th>
								<th>Actions</th>

							</thead>
							<tbody>
							@foreach($paused_gigs as $gig)
							<tr>
								<td>
									<img src="{{url('storage/'.$gig->image)}}" width="50">
								</td>
								
								<td>
									<p>{{$gig->title}}</p><br>
									{{-- <img style="padding-right: 20px;" class="img-circle" width="50" height="50" src="{{asset('storage/'.$paused_gig->image)}}">		 --}}
								</td>
								<td>{{date("F jS, Y", strtotime($gig->created_at))}}</td>
								<td>
									<button class="btn btn-sm btn-success" @click="activateGig({{$gig->id}})">Activate</button>
								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						@if($paused_gigs->count() < 1)
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
					<v-tab title="Cancelled">
						<table class="table table-striped">
							<thead>
								<th></th>
								<th>Title</th>
								<th>Created</th>
							</thead>
							<tbody>
							@foreach($cancelled_gigs as $gig)
							<tr>
								<td>
									<img src="{{url('storage/'.$gig->image)}}" width="50">
								</td>
								
								<td>
									<p>{{$gig->title}}</p><br>
									{{-- <img style="padding-right: 20px;" class="img-circle" width="50" height="50" src="{{asset('storage/'.$paused_gig->image)}}">		 --}}
								</td>
								<td>{{date("F jS, Y", strtotime($gig->created_at))}}</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						@if($cancelled_gigs->count() < 1)
							<div style="text-align: center">
								<p>No records found</p>
							</div>
						@endif
					</v-tab>
				</vue-tabs>
			</div>
		
		</div>
	</div>
@endsection