@extends('layouts.app')
@section('content')
	<div class="container card create-gig-container">
		<h3>Edit Gig</h3>
		<form class="form-horizontal" role="form" action="/gigs/edit/{{$gig->id}}" method="POST" enctype="multipart/form-data">
			<div class="gig-create-form">
				{{csrf_field()}}
					<div>Overview</div>
					<div class="form-group">
						<label for="gig-title" class="col-sm-2 control-label">Gig Title</label>
						<div class="col-sm-10">
							<textarea :value="'{{$gig->title}}'" class="form-control gig-title" id="gig-title" name="gig-title" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-sm-2 control-label">Category</label>
						<div class="col-sm-10">
							<select class="form-control" id="category" name="category" v-model="categorySelected" required>
								<option disabled>Select Category</option>
								<option v-for="i in categories" :key="i.id" :value="i.id">
									@{{i.name}}
								</option>
							</select>
						</div>
					</div>
					<div v-if="categorySelected">
						<div class="form-group">
							<label for="sub-category" class="col-sm-2 control-label">Sub Category</label>
							<div class="col-sm-10">
								<select placeholder="Select a sub category" class="form-control" id="sub-category" name="sub-category" v-model="subCategorySelected" required>
									<option disabled>Select sub category</option>
									<option v-for="i in subcategories" :key="i.id" :value="i.id">
										@{{i.name}}
									</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="search-tags" class="col-sm-2 control-label">Search Tags</label>
						<div class="col-sm-10">
							{{-- <input type="text" name="search-tags" id="search-tags" class="form-control" placeholder="Enter search tags seperated by commas" required> --}}
							<el-tag
							  :key="tag"
							  v-for="tag in dynamicTags"
							  closable
							  :disable-transitions="false"
							  @close="handleClose(tag)">
							  @{{tag}}
							</el-tag>
							<el-input
							  class="input-new-tag"
							  v-if="inputVisible"
							  v-model="inputValue"
							  ref="saveTagInput"
							  size="mini"
							  @keyup.enter.native="handleInputConfirm"
							  @blur="handleInputConfirm"
							>
							</el-input>
							<el-button v-else class="button-new-tag" size="small" @click="showInput">+ New Tag</el-button>
						</div>
					</div>
			</div>
			<div class="gig-create-form">
				<div>Scope and Pricing</div>
				<div class="form-group">
					<label for="delivery-period" class="col-sm-2 control-label">Delivery Period</label>
					<div class="col-sm-10">
						<select class="form-control" name="delivery-period">
							<option value="1">1 day</option>
							@for($i=2; $i<=29; $i++)
								<option 
								@if($gig->delivery_period == $i) 
									selected 
								@endif value="{{$i}}">{{$i}} days</option>
							@endfor
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="price" class="col-sm-2 control-label">Price</label>
					<div class="col-sm-10">
						<input :value="'{{$gig->price}}'" type="number" name="price" id="price" class="form-control" required>
					</div>
				</div>
			</div>
			<div class="gig-create-form">
				<div>Description</div>
				<div class="form-group">
					<label for="description" class="col-sm-2 control-label">Briefly describe your gig</label>
					<div class="col-sm-10">
						<textarea :value="'{{$gig->description}}'" class="form-control" name="description" required></textarea>
					</div>
				</div>
			</div>
			<div class="gig-create-form">
				<div>Requirements</div>
				<label for="requirements" class="col-sm-2 control-label">Requirement</label>
				<div class="col-sm-10">
					<textarea :value="'{{$gig->requirements}}'" class="form-control" placeholder="Tell buyers what you require to deliver the gig" name="requirements" required></textarea>
				</div>
			</div>
			<div class="gig-create-form">
				<div>Gig Image</div>
				<label for="image" class="col-sm-2 control-label">Upload image</label>
				<div class="col-sm-10">
					<input style="display: block" type="file" id="image" name="image" class="form-control">
				</div>
				<button class="btn btn-success" type="submit">Save</button>
			</div>

		</form>
	</div>
@endsection

@section('scripts')
	<script>
		app.categories = {!! $json_categories !!};
		app.categorySelected = {{$gig->category_id}};
		app.subCategorySelected = {{$gig->sub_category_id}};
	</script>
@endsection