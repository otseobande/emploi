@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h3>Settings</h3>
				<div class="card">
					<vue-tabs>
						<v-tab title="Change password">
							<form class="form-horizontal" role="form" method="post" action="{{url('settings/changepassword')}}"><br>
								{{csrf_field()}}
								 <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
		                            <label for="password" class="col-md-4 control-label">Old password:</label>

		                            <div class="col-md-6">
		                                <input id="old-password" type="password" class="form-control" name="old_password" required>

		                                @if ($errors->has('old_password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('old_password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                        <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
		                            <label for="password" class="col-md-4 control-label">New password:</label>

		                            <div class="col-md-6">
		                                <input id="new-password" type="password" class="form-control" name="new_password" required>

		                                @if ($errors->has('new_password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('new_password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		               			<div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
		                            <label for="password" class="col-md-4 control-label">New password confirmation:</label>

		                            <div class="col-md-6">
		                                <input id="new-password-confirmation" type="password" class="form-control" name="new_password_confirmation" required>

		                                @if ($errors->has('new_password_confirmation'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('new_password_confirmation') }}</strong>
		                                    </span>
		                                @endif

		                            </div>
		                            
		                        </div><br>
								<button type="submit" class="btn btn-primary">Change</button>
							</form>
						</v-tab>
					</vue-tabs>
				</div>
			</div>
		</div>
	</div>
@endsection