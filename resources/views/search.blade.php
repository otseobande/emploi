@extends('layouts.app')

@section('content')
	<div class="container">
			@auth
				<div class="row">
					<h3>Users found</h3>
					@if($users->count() > 0)
						@foreach($users as $user)
							<div class="col-md-2">
								<div class="card user-block">
									<img class="img-circle img-responsive" width="50" src="{{ asset($user->profile_image_link)}}" style="margin: auto;">
									<h5>{{$user->username}}</h5>
									<a class="btn btn-primary" href="{{url('profile/'.$user->username)}}">view profile</a>
								</div>
							</div>
						@endforeach
					@else
						<p>No records found</p>
					@endif
				</div>
			@endauth
			<div class="row">
				<h3>Gigs related to '{{$query}}'</h3>
				@if($gigs->count() > 0)
					@foreach($gigs as $gig)
						<div class="col-md-2">	
							<div class="card panel" id="featured-job-panel">
								<div class="panel-heading">
									<img src="{{ asset('storage/'.$gig->image)}}">
								</div>
								<div class="panel-body">
									<div class="featured-user">
										<img class="img-circle img-responsive" src="{{ asset('img/profile/user.png')}}">
										 <p style="margin-left: 2px;">{{$gig->user->username}}</p>
									</div>
									<div class="featured-title">
										{{$gig->title}}
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<p>No records found</p>
				@endif
			</div>
	
	</div>
@endsection