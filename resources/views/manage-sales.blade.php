@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h3><strong>Manage Sales</strong></h3>
			<div class="card">
				<vue-tabs>
					<v-tab title="Active">
						<table class="table table-striped table-hover">
							<thead>
								<th>Gig</th>
								<th>Price</th>
								<th>Delivery date</th>
							</thead>
							<tbody>
								<td></td>
								<td></td>
								<td></td>
							</tbody>
						</table>
					</v-tab>
					<v-tab title="Late"></v-tab>
					<v-tab title="Delivered"></v-tab>
					<v-tab title="Completed"></v-tab>
					<v-tab title="Cancelled"></v-tab>
				</vue-tabs>
			</div>
		</div>
	</div>
@endsection