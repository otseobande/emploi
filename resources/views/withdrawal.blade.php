@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row add-funds-box">
		@if (session('status'))
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif
			<el-card>
				<div v-if="withdrawal.status.sendingWithdrawal" style="text-align: center;">
					<h5>Please wait...</h5>
					<img width="50" src="{{asset('img/rolling.gif')}}">
				</div>
				<div v-else>
					<div v-if="!withdrawal.status.withdrawalSuccessful">
						<form class="form">
							<h4><b>Select bank account:</b></h4>
							<div>
								@foreach($bank_details as $bank_detail)
									<input type="radio" name="bank" v-model="withdrawal.account" value="{{$bank_detail->id}}">
									<label>
										{{$bank_detail->account_no}}
										@foreach($banks as $code => $bank)
											@if($bank_detail->bank_code == $code)
												({{$bank}})
											@endif
										@endforeach
									</label>
									<br>
								@endforeach
							</div><br>

							<div :class="{'form-group': true, 'has-error': errors.has('amount')}">
								<label for="amount">Amount:</label>
								<div class="input-group">
									<span class="input-group-addon">&#8358;</span>
									<input v-validate="'required|numeric'" class="form-control" v-model="withdrawal.amount" label="Amount" placeholder="Enter amount to withdraw" type="number" name="amount" id="amount">
									
								</div>
								<span v-show="errors.has('amount')" class="help-block">
				                        <strong>@{{ errors.first('amount') }}</strong>
				                    </span><br>
							</div>
							<button class="btn btn-success" :disabled="withdrawal.amount > accountBalance || !withdrawal.account || !withdrawal.amount || errors.any()" type="button" @click.prevent="withdraw" style="color: white;">Continue</button>

							<a class="btn btn-primary" href="{{ url('payments/addaccount') }}" style="color: black;">
								Add bank account
							</a>
						</form>
					</div>
					<div v-else>
						<h2>Withdrawal made successfully!</h2>
					</div>
				</div>
			</el-card>
	</div>
</div>
@endsection