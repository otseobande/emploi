@extends('layouts.app')

@section('content')
<div class="container">
	<center>
		<h3>{{$category->name}}</h3>
		<p>Services</p>
	</center>
	<div class="row">
		<div class="col-md-3 hidden-sm">
			<div class="content-box">
				<h4>{{$category->name}}</h4>
				<ul>
					@foreach($category->subcategories()->get() as $subcategory)
						<li>{{$subcategory->name}}</li>
					@endforeach
				</ul>
				<hr>
				<p>{{$category->description}}</p>
			</div>
		</div>
		<div class="col-md-9">
			<div class="row">
				@if($category->gigs()->count() > 1)
					@foreach($category->gigs()->get() as $gig)
						<div class="col-md-3">	
							<div class="panel" id="featured-job-panel">
								<div class="panel-heading">
									<img src="{{ asset('storage/'.$gig->image)}}">
								</div>
								<div class="panel-body">
									<div class="featured-user">
										<img class="img-circle img-responsive" src="{{ asset('img/user.png')}}">
										<p>{{$gig->user->username}}</p>
									</div>
									<div class="featured-title">
										{{$gig->title}}
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<p style="text-align: center;">No Gig has been posted in this category</p>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection