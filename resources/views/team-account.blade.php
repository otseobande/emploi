@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="card" style="text-align: center" v-if="!groupCreated">
				<h2><b>group Account</b></h2>
				<img src="img/group-icon.png">
				<br><br>
				<form class="form-inline" @submit.prevent="creategroup">
					<input class="form-control" type="text" name="group" placeholder="group Name">
					<button type="submit" class="btn btn-success">Create group</button>
				</form>
				<br>
				<h3>Two heads are better than one, get more done together.</h3>
				<p>groups allows you to add existing Emploi users to the same account
You can create a group and fund it with a balance for all group members to purchase from.</p>
			</div>
			<div v-else>
				<h2>group Account</h2>
				<div class="card">
					<div class="row">
						<div class="col-md-6">
							<p><b>GROUP NAME</b></p>
						</div>
						<div class="col-md-6">
							<p>Test</p>	
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<p><b>AVAILABLE BALANCE</b></p>
						</div>
						<div class="col-md-6">
							<p>N0.00</p><button class="btn btn-success">Add funds</button>	
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<p><b>INVITE MEMBER</b></p>
						</div>
						<div class="col-md-6">
							<form class="form-inline">
								<input type="text" placeholder="username or email address of user" name="" class="form-control">
								<button type="submit" class="btn">Add User</button>
							</form>	
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<p><b>MANAGE MEMBERS</b></p>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<p><b>Email</b></p>
								</div>
								<div class="col-md-6">
									<p><b>Role</b></p>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<p>ot*****de@gmail.com</p>
								</div>
								<div class="col-md-6">
									<p>Admin</p>
								</div>
								<hr>
							</div>
						</div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>
@endsection