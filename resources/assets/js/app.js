require('./bootstrap');

window.Vue = require('vue');
import VueTabs from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import VeeValidate from 'vee-validate';
import Countdown from './components/countdown.vue';

//window.moment = Moment;

Vue.component('countdown', Countdown);
Vue.use(VeeValidate);
Vue.use(ElementUI, { locale })
Vue.use(VueTabs);
// import N3Components from 'N3-components'
// import 'N3-components/dist/index.css'

// Vue.use(N3Components)

window.app = new Vue({
    el: '#app',
    data:{
      //now: Moment.now(),
      deliveredFile: 0,
      currentOrder: 0,
      accountBalance: 0,
      selectedOrderId: 0, 
      modificationRequestDialogVisible: false,
      employerCancelOrderDialogVisible: false,
      employeeCancelOrderDialogVisible: false,
      deliverDialogVisible: false,
      gigOrderDialogVisible: false,
      customOfferDialogVisible: false,
    	teamCreated: false,
    	categories: null,
    	categorySelected: '',
      subCategorySelected: '',
    	edittingDescription: false,
    	edittingCertification: false,
    	edittingEducation: false,
    	edittingSkills: false,
      showCategories: false,
      modificationReason: '',
      fileList: [],
      message: {
        text: '',
        files: [],
      },
      gig:{
        title: '',
      },
      payment: {
        details:{
          cardNo: "",
          cvv: "",
          expiryMonth: "",
          expiryYear: "",
          pin: "",
          amount: "",
          otp: "",
          ref: "",
        },
        status:{
          otpSent: false,
          otpValid: false,
          sendingOtp: false,
          validatingCard: false,
          cardValid: false,
          amountValid: false,
        },
        response: "",
        error: "",
      },
      withdrawal:{
        account: "",
        amount: "",
        status:{
          sendingWithdrawal: false,
          withdrawalSuccessful: false,
        }
      },
      bankAccount:{
        accountNo: "",
        bankCode: "",
        accountName: "",
      },
      skills: [],
      dynamicTags: [],
        inputVisible: false,
        inputValue: '',
        skillInput: '',
    },
    methods:{
      requestModification(){
        if(confirm('Are you sure you want to push this delivery for revision?')){
          axios.post(`/orders/requestmodification/${this.selectedOrderId}`,{
            modificationReason : this.modificationReason,
          }).then((res=>{
            location.reload();
          }).bind(this)).catch((err=>{
            console.log(err);
            console.log(err.response);
          }).bind(this));
        }
      },
      showRequestModificationDialog(orderId){
        this.modificationRequestDialogVisible = true;
        this.selectedOrderId = orderId;
      },
      showCancelOrderDialog(orderId){
        this.employerCancelOrderDialogVisible = true;
        this.selectedOrderId = orderId;
      },
      cancelOrder(){
        axios.post(`/orders/cancel/${this.selectedOrderId}`).then((res=>{
          location.reload();
        }).bind(this)).catch((err=>{
          console.log(err);
          console.log(err.response.data)
        }))
      },
      acceptCustomOffer(offerId){
        if(confirm('Are you sure you want to accept this offer?')){
          axios.post(`/offer/accept/${offerId}`).then((res=>{
            location.reload();
          }).bind(this)).catch((err=>{
            console.log(err);
            console.log(err.response.data);
          }).bind(this));
        }
      },
      withdrawCustomOffer(offerId){
        if(confirm('Are you sure you want to withdraw this offer?')){
          axios.post(`/offer/withdraw/${offerId}`).then((res=>{
            location.reload();
          }).bind(this)).catch((err=>{
            console.log(err);
            console.log(err.response.data);
          }).bind(this))
        }
      },
      addIWillToGigTitle(){
        if(this.gig.title.slice(0,6).toLowerCase() != "i will"){
          this.gig.title = "I will " + this.gig.title; 
        }
      },
      removeSkill(skillId){
        if(confirm('Are you sure you want to remove skill?')){
          axios.post(`/removeskill/${skillId}`).then((res=>{
            for(let i = 0; i < this.skills.length; i++){
              if(this.skills[i].id == skillId){
                delete this.skills[i];
                this.skills = this.skills.filter(val=>val);
                break;
              }
            }
            
          }).bind(this)).catch((err=>{
            console.log(err);
            console.log(err.response.data);
          }).bind(this));
        }
      },
      uploadedFile(){
        this.deliveredFile = document.querySelector('#delivery').files.length;
      },
      showDeliveryDialog(id){
        this.deliverDialogVisible = true;
        this.currentOrder = id;
      },
      deliverOrder(){
        let form = new FormData(this.$refs.deliveryForm);
        axios.post(`/orders/deliver/${this.currentOrder}`,form).then((res =>{
          location.reload();
        }).bind(this)).catch((err => {
          console.log(err);
          console.log(err.response.data);
        }).bind(this))
      },
      displayUploadDialog(){
        let uploadInput = document.querySelector('#profile-pic-upload');
        uploadInput.click();
      },
      changeProfilePic(){
        let form = new FormData(this.$refs.profilePicUploadForm);
        axios.post('/profile/changeprofilepicture',form).then(res=>{
          //console.log(res.data);
          location.reload();
        }).catch(err=>{
          console.log(err);
          console.log(err.response.data);
        })
      },
      download(path){
        
      },
      orderGig(gigId){
        axios.post(`/orders/gig/${gigId}`).then(res=>{
          location.href = '/orders/manage';
        }).catch(err=>{
          console.log(err);
          console.log(err.response.data);
        })
      },
      validateAmount(){
        this.payment.status.amountValid = true;
      },
      validateCard(){
          this.payment.status.validatingCard = true;
          axios.post('/payments/validate', this.payment.details).then((res=>{
            let response = res.data;
            this.payment.response = response;
            this.payment.status.validatingCard = false;
            if(response.status == "success"){
              this.payment.status.otpValid = true;
            }else{
              this.payment.error = this.payment.response.message;
            }
            
          }).bind(this)).catch((err=>{
            this.payment.status.validatingCard = false;
            this.payment.response = err.response.data;
            console.log(err.response.data);
          }).bind(this));
        
      },
      addFunds(){
          this.payment.status.sendingOtp = true;
          axios.post('/payments/addfunds',this.payment.details).then(((res)=>{
            let response = res.data;
            this.payment.status.sendingOtp = false;   
            this.payment.response = response;
            if(response.status == "success"){
              this.payment.status.otpSent = true;
              this.payment.status.cardValid = true;
              this.payment.error = null;
              this.payment.details.ref = response.data.transfer.flutterChargeReference;
            }else{
              this.payment.error = response.message;
            }
          }).bind(this)).catch(((err)=>{
            this.payment.response = err.response.data;
            this.payment.error = err.response.message;
            this.payment.status.sendingOtp = false;
            this.payment.status.otpSent = false;
          }).bind(this));
        
      },
      withdraw(){
        this.withdrawal.status.sendingWithdrawal = true;
        axios.post('/payments/withdraw', this.withdrawal).then(((res)=>{
          let response = res.data;
          this.withdrawal.status.sendingWithdrawal = false;
          this.withdrawal.status.withdrawalSuccessful = response.status == "success";
        }).bind(this)).catch((err =>{
          console.log(err);
          console.log(err.response.data);
          this.withdrawal.status.sendingWithdrawal = false;
        }).bind(this));
      },
      acceptOffer(offerId){
        if(confirm('Are you sure you want to accept this offer?')){
          axios.post(`/requests/offer/accept/${offerId}`).then(res=>{
            location.href = '/manage_orders';
          }).catch(err=>{
            console.log(err);
            console.log(err.response.data);
          })
        }
      },
      activateGig(gigId){
        if(confirm('Are you sure you want to activate gig')){
          axios.post(`/gigs/activate/${gigId}`).then(res=>{
            location.reload();
          }).catch(err=>{
            console.log(err);
            console.log(err.response.data);
          })
        }
      },
      pauseGig(gigId){
        if(confirm('Are you sure you want to pause gig?')){
          axios.post(`/gigs/pause/${gigId}`).then(res=>{
            location.reload();
          }).catch(err=>{
            console.log(err);
            console.log(err.response.data);
          })
        }
      },
      cancelGig(gigId){
          if(confirm('Are you sure you want to cancel gig?')){
            axios.post(`/gigs/cancel/${gigId}`).then(res=>{
              location.reload();
            }).catch(err=>{
              console.log(err);
              console.log(err.response.data);
            })
          }
      },
      handleClose(tag) {
        this.dynamicTags.splice(this.dynamicTags.indexOf(tag), 1);
      },

      showInput() {
        this.inputVisible = true;
        this.$nextTick(_ => {
          this.$refs.saveTagInput.$refs.input.focus();
        });
      },

      handleInputConfirm() {
        let inputValue = this.inputValue;
        if (inputValue) {
          this.dynamicTags.push(inputValue);
        }
        this.inputVisible = false;
        this.inputValue = '';
      },
      redirect(link){
        location.href = link;
      },
      sendMessage(username){
        let form = this.$refs.messageForm;
        let formdata = new FormData(form);
        if(this.message.text){
          axios.post(`/conversations/${username}/send`,formdata).then((res)=>{
            location.reload();
            //console.log(res.data);
          }).catch(err=>{
            console.log(err);
            console.log(err.response.data);
          });
        }else{
          alert("Message cannot be empty");
        }
      },
      uploadMessageAttachment(){
        let uploadInput = document.querySelector('#attachment-upload');
        if(uploadInput){
          uploadInput.click();
        }
      },
      bindUploadedFiles(){
        let uploadInput = document.querySelector('#attachment-upload');
        if(uploadInput){
          this.message.files = uploadInput.files
        }
      },
      toggleShowCategories(){
         this.showCategories = !this.showCategories
      },
      hideCategories(){
        this.showCategories = false;
      },
    	createTeam(){
    		this.teamCreated = true;
    	},
    	openConversation(username,threadId){
    		location.href = '/conversations/'+username;
    	},
    	cancelOffer(requestId){
    		if(confirm('Are you sure you want to delete offer?')){
	    		axios.post('/requests/offer/'+requestId+'/cancel').then((res)=>{
	    			console.log(res.data);
	    			location.reload();
	    		}).catch((err)=>{
	    			console.log(err.response.data);
	    		})
    		}
    	},
    	suspendRequest(id){
    		if(confirm('Are you sure you want to suspend?')){
	    		axios.post('/requests/suspend/'+id).then((res)=>{
	    			console.log(res.data);
	    			location.reload();
	    		}).catch((err)=>{
	    			console.log(err.response.data);
	    		})
    		}
    	},
    	activateRequest(id){
    		if(confirm('Are you sure you want to activate?')){
	    		axios.post('/requests/activate/'+id).then((res)=>{
	    			console.log(res.data);
	    			location.reload();
	    		}).catch((err)=>{
	    			console.log(err.response.data);
	    		})
    		}
    	}
    },
    computed:{
    	subcategories(){
    		for(let i of this.categories){
    			if(i.id == this.categorySelected){
    				return i.subcategories;
    			}
    		}
    	},

    }
});
