<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_offers', function (Blueprint $table) {
           $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('employer_id')->unsigned();
            $table->integer('gig_id')->unsigned();
            $table->string('details');
            $table->double('price',10,2);
            $table->integer('delivery_period');
            $table->integer('revisions')->nullable();
            $table->boolean('accepted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_offers');
    }
}
